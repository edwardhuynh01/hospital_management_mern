import React, { useEffect, useReducer } from "react";
import MedicalReportList from "../../../components/ProfileManager/MedicalReport/MedicalReportList";
import NotificationList from "../../../components/ProfileManager/Notification/NotificationList";
import HospitalFeePaymentHistoriesList from "../../../components/ProfileManager/HospitalFeePaymentHistory/HospitalFeePaymentHistoriesList";
import styled from "styled-components";
import { jwtDecode } from "jwt-decode";
import {
  IoPersonAdd,
  IoIdCardOutline,
  IoDocumentTextOutline,
  IoNotificationsOutline,
  IoTimerOutline,
} from "react-icons/io5";
import MedicalRecordsList from "../../../components/ProfileManager/MedicalRecords/MedicalRecordsList";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
const BtnMakeRCMedical = styled.a`
  width: 100%;
  height: 2.5em;
  border-radius: 24px;
  border: 1px solid #d1d1d1;
  background: rgba(255, 104, 159, 0.75);
  box-shadow: 0px 15px 30px -10px rgba(255, 189, 213, 0.3);
  color: #fff;
  text-align: center;
  font-size: 1em;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  transition: color 0.25s, border-color 0.25s, box-shadow 0.25s, transform 0.25s;
  cursor: pointer;
  padding: 0.5rem 1rem;
  align-items: center;
  margin-top: 20px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  &:hover {
    border-color: rgba(255, 104, 159, 0.75);
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
const BtnProfile = styled.button`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
`;
const initialState = {
  isMedicalRecordsClick: true,
  isMedicalReportClick: false,
  isNotificationClick: false,
  isHospitalFeePaymentHistoryClick: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "CLICK_MEDICALRECORDS":
      return {
        ...state,
        isMedicalRecordsClick: true,
        isMedicalReportClick: false,
        isNotificationClick: false,
        isHospitalFeePaymentHistoryClick: false,
      };
    case "CLICK_MEDICALREPORT":
      return {
        ...state,
        isMedicalReportClick: true,
        isNotificationClick: false,
        isHospitalFeePaymentHistoryClick: false,
        isMedicalRecordsClick: false,
      };
    case "CLICK_NOTIFICATION":
      return {
        ...state,
        isNotificationClick: true,
        isHospitalFeePaymentHistoryClick: false,
        isMedicalRecordsClick: false,
        isMedicalReportClick: false,
      };
    case "CLICK_HOSPITALFEEPAYMENTHISTORY":
      return {
        ...state,
        isHospitalFeePaymentHistoryClick: true,
        isMedicalRecordsClick: false,
        isMedicalReportClick: false,
        isNotificationClick: false,
      };
    default:
      return state;
  }
};
function ProfileManager() {
  const location = useLocation();
  useEffect(() => {
    if (location.state?.message) {
      toast(location.state.message);
    }
  }, [location.state]);
  const [state, dispatch] = useReducer(reducer, initialState);
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigate = useNavigate();
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const query = new URLSearchParams(location.search);
  const component = query.get("component");
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  });
  useEffect(() => {
    switch (component) {
      case "medicalRecords":
        dispatch({ type: "CLICK_MEDICALRECORDS" });
        break;
      case "medicalReports":
        dispatch({ type: "CLICK_MEDICALREPORT" });
        break;
      case "notifications":
        dispatch({ type: "CLICK_NOTIFICATION" });
        break;
      case "paymentHistory":
        dispatch({ type: "CLICK_HOSPITALFEEPAYMENTHISTORY" });
        break;
      default:
        dispatch({ type: "CLICK_MEDICALRECORDS" });
    }
  }, [component]);
  const { id } = useParams();
  return (
    <div className="flex justify-center py-10 bg-white">
      <div className="flex w-3/4 flex-wrap">
        <div className="flex lg:w-2/6 flex-col">
          <div className="text-center mb-7">
            <BtnMakeRCMedical href="/profile/MakeMedicalRC">
              <div className="mx-3">
                <IoPersonAdd></IoPersonAdd>
              </div>
              Thêm hồ sơ bệnh nhân
            </BtnMakeRCMedical>
          </div>
          <div className="w-full ">
            <BtnProfile
              // onClick={handleMedicalRecordsClick}
              onClick={() => navigate("/profile?component=medicalRecords")}
              className={`${
                state.isMedicalRecordsClick
                  ? "bg-[#ffe5ec] text-[#fb6f92]"
                  : "hover:bg-[#f2f2f2]"
              }  flex justify-start items-center w-full text-start h-10 rounded-e-2xl active:bg-[#ffcad4]`}
            >
              <div className="mx-3">
                <IoIdCardOutline></IoIdCardOutline>
              </div>
              Hồ sơ bệnh nhân
            </BtnProfile>
            <BtnProfile
              // onClick={handleMedicalReportClick}
              onClick={() => navigate("/profile?component=medicalReports")}
              className={`${
                state.isMedicalReportClick
                  ? "bg-[#ffe5ec] text-[#fb6f92]"
                  : "hover:bg-[#f2f2f2]"
              }  flex justify-start items-center w-full text-start h-10 rounded-e-2xl active:bg-[#ffcad4]`}
            >
              <div className="mx-3">
                <IoDocumentTextOutline></IoDocumentTextOutline>
              </div>
              Phiếu khám bệnh
            </BtnProfile>
            <BtnProfile
              // onClick={handleNotificationClick}
              onClick={() => navigate("/profile?component=notifications")}
              className={`${
                state.isNotificationClick
                  ? "bg-[#ffe5ec] text-[#fb6f92]"
                  : "hover:bg-[#f2f2f2]"
              }  flex justify-start items-center w-full text-start h-10 rounded-e-2xl active:bg-[#ffcad4]`}
            >
              <div className="mx-3">
                <IoNotificationsOutline></IoNotificationsOutline>
              </div>
              Thông báo
            </BtnProfile>
            <BtnProfile
              // onClick={handleHospitalFeePaymentHistoryClickClick}
              onClick={() => navigate("/profile?component=paymentHistory")}
              className={`${
                state.isHospitalFeePaymentHistoryClick
                  ? "bg-[#ffe5ec] text-[#fb6f92]"
                  : "hover:bg-[#f2f2f2]"
              }  flex justify-start items-center w-full text-start h-10 rounded-e-2xl active:bg-[#ffcad4]`}
            >
              <div className="mx-3">
                <IoTimerOutline></IoTimerOutline>
              </div>
              Lịch sử thanh toán viện phí
            </BtnProfile>
          </div>
        </div>
        <div className="lg:w-4/6 overflow-y-auto md:max-h-96">
          {state.isMedicalRecordsClick && <MedicalRecordsList />}
          {state.isMedicalReportClick && <MedicalReportList />}
          {state.isNotificationClick && <NotificationList />}
          {state.isHospitalFeePaymentHistoryClick && (
            <HospitalFeePaymentHistoriesList />
          )}
        </div>
      </div>
    </div>
  );
}

export default ProfileManager;
