import React from 'react';
import Title from '../../../components/Title/Title';
import { IoLocationOutline, IoCallOutline, IoMailOutline, IoGlobeOutline } from "react-icons/io5";
const ContactPage = () => {
    return (
        <div className='bg-white'>
            <div class="container mx-auto">
                <Title>LIÊN HỆ</Title>
                <div class="flex justify-center items-center mt-6">
                    <div className='lg:w-1/2 w-full h-full ml-28 mb-28 bg-[#e9ecef] h-full'>
                        <div className=''>
                            <div className='m-5 pl-8 pr-3 pb-3 pt-3 px-3 bg-main-gradient-to-left'>
                                <div class=" flex justify-center w-full">
                                    <img className='w-36 h-36' src="/sinhto.png" alt="Logo bệnh viện" />
                                </div>
                                <div class="">
                                    <div className='text-center text-3xl text-white font-bold mb-6'><p>BỆNH VIỆN ĐA KHOA SINH TỐ DÂU</p></div>
                                    <ul className='p-4'>
                                        <li className='flex justify-start items-center leading-10 gap-2 text-white text-lg'><IoLocationOutline className='text-2xl'/>Lô E2 Khu Công nghệ cao, TP. Thủ Đức</li>
                                        <li className='flex justify-start items-center leading-10 gap-2 text-white text-lg'><IoCallOutline className='text-2xl'/>(028) 3666 9555</li>
                                        <li className='flex justify-start items-center leading-10 gap-2 text-white text-lg'><IoMailOutline className='text-2xl'/>stdhospital@gmail.com</li>
                                        <li className='flex justify-start items-center leading-10 gap-2 text-white text-lg'><IoGlobeOutline className='text-2xl'/>bvsinhtodau.vn</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-1/2 h-full mr-28 mb-28">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d979.6071450865865!2d106.78485486855752!3d10.85497115229025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317527c3debb5aad%3A0x5fb58956eb4194d0!2zxJDhuqFpIEjhu41jIEh1dGVjaCBLaHUgRQ!5e0!3m2!1svi!2s!4v1712655140581!5m2!1svi!2s" width="600" height="450" style={{ border: '0' }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactPage;