import React, { useEffect, useRef, useState } from "react";
import HeaderButton from "../../../../components/Button/HeaderButton";
import { IonIcon } from "@ionic/react";
import { menuSharp, closeSharp, logOut } from "ionicons/icons";
import { IoChevronDownOutline, IoPersonCircle } from "react-icons/io5";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../../../redux/apiRequest";
import { logoutSuccess } from "../../../../redux/authSlice";
import { createAxios } from "../../../../createInstance";
import HeaderProfile from "../../../../components/Button/HeaderProfile";
import serviceApi from "../../../../api/servicesApi";
import specialistApi from "../../../../api/specialistApi";
import newsApi from "../../../../api/newsApi";
import servicesApi from "../../../../api/servicesApi";
const Header = () => {
  const [currentIcon, setCurrentIcon] = useState(menuSharp);
  const [dropdownHover, setDropdownHover] = useState(false);
  const [dropdownClick, setDropdownClick] = useState(false);
  const [dropdownUser, setDropdownUser] = useState(false);
  const user = useSelector((state) => state.auth.login?.currentUser);
  const handleMouseEnter = () => {
    setDropdownHover(true);
  };
  const handleMouseLeave = () => {
    setDropdownHover(false);
  };
  const handleDropdownClick = () => {
    setDropdownClick(!dropdownClick);
  };
  const ref = useRef(null);
  const navRef = useRef(null);

  const toggleMenu = () => {
    setDropdownClick(false);
    if (navRef.current) {
      if (currentIcon === menuSharp) {
        setCurrentIcon(closeSharp);
        navRef.current.classList.remove("<=lg:hidden");
        setDropdownUser(true);
      } else {
        setCurrentIcon(menuSharp);
        setDropdownUser(false);
        navRef.current.classList.add("<=lg:hidden");
      }
    }
  };
  const handleUserClick = () => {
    setDropdownUser(!dropdownUser);
  };
  const DISPATCH = useDispatch();
  const navigate = useNavigate();
  const accessToken = user?.accessToken;
  const id = user?._id;
  let axiosJWT = createAxios(user, DISPATCH, logoutSuccess);
  const handleLogout = () => {
    toggleMenu();
    logout(DISPATCH, id, navigate, accessToken, axiosJWT);
  };

  const encode = (id) => {
    return btoa(id);
  };
  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 1280px)"); // xl: max-width 1280px
    const handleMediaQueryChange = (e) => {
      if (navRef.current) {
        if (e.matches) {
          setDropdownUser(true);
          setDropdownClick(true);
          setCurrentIcon(menuSharp);
          navRef.current.classList.add("<=lg:hidden");
        } else {
          setDropdownUser(false);
          setDropdownClick(false);
          setCurrentIcon(closeSharp);
          navRef.current.classList.remove("<=lg:hidden");
        }
      }
    };
    mediaQuery.addEventListener("change", handleMediaQueryChange);
    handleMediaQueryChange(mediaQuery); // Kiểm tra ngay khi component được mount

    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);
  const handleClickOutside = (event) => {
    const mediaQuery = window.matchMedia("(min-width: 1280px)"); // xl: min-width 1280px
    if (
      mediaQuery.matches &&
      ref.current &&
      !ref.current.contains(event.target)
    ) {
      setDropdownUser(false);
    }
  };
  useEffect(() => {
    const mediaQuery = window.matchMedia("(min-width: 1280px)"); // xl: min-width 1280px
    if (mediaQuery.matches) {
      document.addEventListener("click", handleClickOutside, true);
    }

    return () => {
      if (mediaQuery.matches) {
        document.removeEventListener("click", handleClickOutside, true);
      }
    };
  }, []);

  const [specialists, setSpecialists] = useState([]);
  useEffect(() => {
    const fetchSpecialist = async () => {
      try {
        const data = await specialistApi.getAllSpecialist();
        setSpecialists(data);
      } catch (error) { }
    };
    fetchSpecialist();
  }, []);
  const [keyword, setKeyword] = useState('');
  const [debounceSearch, setDebounceSearch] = useState('');
  const handleInputChange = (e) => {
    setKeyword(e.target.value);
  };
  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      navigate(`/Search/  ${keyword}`);
    }
  };

  useEffect(() => {
    const timeOut = setTimeout(() => {
      setDebounceSearch(keyword);
    }, 500)
    return () => clearTimeout(timeOut);
  }, [keyword])

  const [newsList, setNewsList] = useState([]);
  const [serviceList, setServiceList] = useState([]);
  const [filteredNews, setFilteredNews] = useState([]);
  const [filteredServices, setFilteredServices] = useState([]);
  const fetchNews = async (keyword) => {
    try {
      const response = await newsApi.getNewsByTitle(keyword);
      setNewsList(response);
    } catch (error) {
      console.error("Error fetching news:", error);
    }
  };
  const fetchServices = async (keyword) => {
    try {
      const data = await servicesApi.getServiceByName(keyword);
      setServiceList(data);
    } catch (error) {
      console.error("Error fetching services:", error);
    }
  };
  useEffect(() => {
    if (debounceSearch) {
      fetchServices(debounceSearch);
      fetchNews(debounceSearch);
    } else {
      setNewsList([]);
      setServiceList([]);
    }
  }, [debounceSearch])
  console.log(newsList);
  console.log(serviceList);

  useEffect(() => {
    const filteredNews = newsList.filter((news) =>
      news.title.toLowerCase().trim().includes(debounceSearch.toLowerCase().trim())
    );
    setFilteredNews(filteredNews);
    const filteredServices = serviceList.filter((service) =>
      service.name.toLowerCase().trim().includes(debounceSearch.toLowerCase().trim())
    );
    setFilteredServices(filteredServices);
  }, [debounceSearch, newsList, serviceList]);
  const isHaveBoth =
    Array.isArray(filteredServices) &&
    Array.isArray(filteredNews) &&
    filteredServices.length > 0 &&
    filteredNews.length > 0;
  const isServiceNull =
    Array.isArray(filteredServices) && filteredServices.length > 0;
  const isNewsNull = Array.isArray(filteredNews) && filteredNews.length > 0;





  const itemsColTop =
    "<=lg:w-full <=lg:h-auto <=lg:flex <=lg:items-center <=lg:justify-center ";
  const liClass =
    "flex text-center items-center <=lg:border-t <=lg:pt-4 <=lg:border-solid <=lg:border-t-bg-slate-200 justify-start <=lg:text-[#343a40] <=lg:font-semibold text-[#FB6F92] text-lg font-bold px-4 xl:hover:bg-[#ffc2d1] <=lg:my-0 <=lg:w-full <=lg:my-2 <=lg:h-auto";
  return (
    <div className="">
      <header className="static w-auto h-32 2xl:px-[100px] flex items-center border-b-2 shadow-sm bg-white">
        <div className="container h-full mx-auto px-3 flex <=lg:flex-wrap flex-nowrap items-center">
          <div className="w-1/4 h-full <=lg:w-full <=lg:h-full <=lg:flex <=lg:justify-between <=lg:items-center">
            <a className="cursor-pointer w-full h-full <=lg:w-auto" href="/">
              <img
                className="w-full h-full <=lg:h-full <=lg:w-full"
                src="\images\LogoTeam.png"
                alt="Logo"
              />
            </a>
            <span className="cursor-pointer xl:hidden">
              <IonIcon
                className={`h-8 w-8 border-2 border-solid border-black rounded-lg ${currentIcon === menuSharp
                  ? "transition  duration-1000 ease-out delay-1000 "
                  : ""
                  }`}
                icon={currentIcon}
                onClick={toggleMenu}
              ></IonIcon>
            </span>
          </div>
          <nav
            ref={navRef}
            className=" w-3/4 h-full <=lg:flex <=lg:items-center <=lg:absolute <=lg:flex-wrap <=lg:z-10 <=lg:w-[30%] <=md:w-[80%]
            left-0 top-32 <=lg:overflow-y-auto <=lg:max-h-[400px]"
          >
            <div className="h-1/2 flex justify-end items-center <=lg:flex-wrap <=lg:w-full <=lg:h-auto <=lg:bg-slate-50 ">
              <div className={`${itemsColTop} relative`}>
                <input
                  type="text"
                  className="mx-4 text-lg border border-solid border-[#ffc2d1] rounded-md px-2 py-1 <=lg:h-auto <=lg:w-auto <=lg:mx-0 
                  <=lg:py-0 <=lg:mt-2 placeholder-[#ff8fab] caret-[#ff8fab] focus:border-[#ff8fab] focus:outline-none focus:placeholder-[#ff8fab]
                  focus:text-[#fb6f92] text-[#fb6f92]"
                  name="keyword"
                  placeholder="Tìm kiếm"
                  value={keyword}
                  onChange={handleInputChange}
                  onKeyDown={handleKeyDown}
                />
                <div
                  className={`absolute flex justify-between items-start top-10 left-0 ${isHaveBoth ? 'block' : 'hidden'} z-[9999]
                 bg-pink-100 w-[250%] p-4 rounded-md max-h-[15em] overflow-y-scroll  <=lg:w-[100%]`}>
                  {isHaveBoth && (
                    <div className="flex justify-center items-start space-x-4 <=lg:flex-col <=xl:space-x-0">

                      {/* Danh sách dịch vụ */}
                      <div className="flex flex-col gap-2">
                        <h2 className="text-center text-[#7b2cbf]">Service</h2>
                        <div className="flex flex-col items-center gap-2">
                          {serviceList?.map((serItem) => (
                            <div key={serItem?._id} className="w-full group">
                              <Link
                                className="w-full flex justify-start items-center gap-2"
                                to={`/service/${encode(serItem?._id)}`}>
                                <img
                                  className="w-14 h-14 rounded-md"
                                  src={`data:image/png;base64,${serItem?.mainImage}`}
                                  alt={serItem?.name}
                                />
                                <p className="text-sm text-[#c77dff] group-hover:text-[#9d4edd]">{serItem?.name}</p>
                              </Link>
                            </div>
                          ))}
                        </div>
                      </div>

                      {/* Danh sách tin tức */}
                      <div className="flex flex-col gap-2">
                        <h2 className="text-center text-[#7b2cbf]">New</h2>
                        <div className="flex flex-col items-center ">
                          {newsList?.map((newsItem) => (
                            <div key={newsItem?._id} className="w-full group ">
                              <Link
                                className="w-full flex justify-start items-center gap-2"
                                to={`/news/${encode(newsItem?._id)}`}>
                                <img
                                  className="w-14 h-14 rounded-md"
                                  src={`data:image/png;base64,${newsItem?.mainImage}`}
                                  alt={newsItem?.title}
                                />
                                <p className="text-sm text-[#c77dff] group-hover:text-[#9d4edd]">{newsItem?.title}</p>
                              </Link>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                {
                  !isHaveBoth && (
                    <>
                      <div
                        className={`absolute flex justify-between items-start top-10 left-0 z-[9999]
                 bg-pink-100 w-[180%] p-4 rounded-md max-h-[15em] overflow-y-scroll ${isServiceNull || isNewsNull ? 'block' : 'hidden'} <=lg:w-[100%]`}>
                        {
                          isServiceNull && (
                            <>
                              <div className="flex flex-col gap-2">
                                <h2 className="text-center text-[#7b2cbf]">Service</h2>
                                <div className="flex flex-col items-center gap-2">
                                  {serviceList?.map((serItem) => (
                                    <div key={serItem?._id} className="w-full group">
                                      <Link
                                        className="w-full flex justify-start items-center gap-2"
                                        to={`/service/${encode(serItem?._id)}`}>
                                        <img
                                          className="w-14 h-14 rounded-md"
                                          src={`data:image/png;base64,${serItem?.mainImage}`}
                                          alt={serItem?.name}
                                        />
                                        <p className="text-sm text-[#c77dff] group-hover:text-[#9d4edd]">{serItem?.name}</p>
                                      </Link>
                                    </div>
                                  ))}
                                </div>
                              </div>
                            </>
                          )
                        }
                        {
                          isNewsNull && (
                            <div className="flex flex-col gap-2">
                              <h2 className="text-center text-[#7b2cbf]">New</h2>
                              <div className="flex flex-col items-center gap-2">
                                {newsList?.map((newsItem) => (
                                  <div key={newsItem?._id} className="w-full group ">
                                    <Link
                                      className="w-full flex justify-start items-center gap-2"
                                      to={`/news/${encode(newsItem?._id)}`}>
                                      <img
                                        className="w-14 h-14 rounded-md"
                                        src={`data:image/png;base64,${newsItem?.mainImage}`}
                                        alt={newsItem?.title}
                                      />
                                      <p className="text-sm text-[#c77dff] group-hover:text-[#9d4edd]">{newsItem?.title}</p>
                                    </Link>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )
                        }
                      </div>
                    </>
                  )

                }



              </div>
              <div className={itemsColTop}>
                <HeaderButton href="/ChooseRecord">ĐĂNG KÍ KHÁM</HeaderButton>
              </div>
              {user ? (
                <div
                  ref={ref}
                  className="<=lg:flex <=lg:justify-center <=lg:flex-col <=lg:w-full"
                >
                  <a
                    className={`<=lg:hidden cursor-pointer flex items-center transition duration-150 ease-in-out delay-100 hover:bg-[#fb6f92] hover:text-white text-[#fb6f92] justify-center gap-3 py-2 px-6 border border-solid border-[#fb6f92] rounded-lg`}
                    onClick={handleUserClick}
                  >
                    <IoPersonCircle />
                    {user?.username}
                  </a>
                  {dropdownUser && (
                    <HeaderProfile
                      logout={handleLogout}
                      name={user?.username}
                      onClick={toggleMenu}
                    />
                  )}
                </div>
              ) : (
                <div className={itemsColTop}>
                  <HeaderButton href="/login">ĐĂNG NHẬP</HeaderButton>
                </div>
              )}
            </div>
            <ul
              className="flex flex-nowrap <=lg:flex-wrap justify-between w-full h-1/2 bg-[#f8f9fa] rounded-se-lg
                          <=lg:rounded-none  <=lg:h-auto <=lg:bg-slate-50 rounded-ss-lg <=lg:py-4"
            >
              <li className={`rounded-ss-lg ${liClass}`}>
                <a
                  className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-10 ease-in-out font-sans"
                  href="/"
                >
                  TRANG CHỦ
                </a>
              </li>
              <li className={liClass}>
                <a
                  className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans"
                  href="/Service"
                >
                  DỊCH VỤ
                </a>
              </li>
              <li
                className={`relative ${liClass}`}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
              >
                <div className="flex items-center justify-between <=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans">
                  <a className="" href="/Specialist">
                    CHUYÊN KHOA
                  </a>
                  <IoChevronDownOutline
                    className="ml-2 xl:pointer-events-none <=lg:cursor-pointer"
                    onClick={handleDropdownClick}
                  />
                </div>
                <div
                  className={`xl:absolute <=lg:hidden xl:${dropdownHover ? "block" : "hidden"
                    } 
                  z-[1] left-0 top-[61px] right-[-120px] bg-main-gradient-to-left rounded-md overflow-hidden`}
                >
                  {specialists?.map((specialistsItem) => (
                    <a
                      href={`/Specialist/${encode(specialistsItem?._id)}`}
                      key={specialistsItem?._id}
                      className="cursor-pointer flex flex-col text-start text-sm font-medium px-4 py-2 text-white hover:bg-[#ff8fab] font-sans"
                    >
                      {specialistsItem?.specialistName}
                    </a>
                  ))}
                </div>
              </li>
              <li
                className={`${dropdownClick ? "block" : "hidden"
                  } flex flex-col text-start text-[#343a40]`}
              >
                {specialists?.map((specialistsItem) => (
                  <a
                    href={`/Specialist/${encode(specialistsItem?._id)}`}
                    key={specialistsItem?._id}
                    className="flex cursor-pointer 
                       text-md mx-4 my-2"
                  >
                    <p className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans">
                      {specialistsItem?.specialistName}
                    </p>
                  </a>
                ))}
              </li>
              <li className={liClass}>
                <a
                  className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans"
                  href="/News"
                >
                  TIN TỨC
                </a>
              </li>
              <li className={liClass}>
                <a
                  className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans"
                  href="/CustomerIGuide"
                >
                  HƯỚNG DẪN KHÁCH HÀNG
                </a>
              </li>
              <li className={`rounded-se-lg ${liClass}`}>
                <a
                  className="<=lg:hover:text-[#ff8fab] transition duration-300 delay-100 ease-in-out font-sans"
                  href="/contact"
                >
                  LIÊN HỆ
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
};

export default Header;
