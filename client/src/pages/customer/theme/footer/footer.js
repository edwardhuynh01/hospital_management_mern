import React from 'react';

const Footer = () => {
    const liClass = 'text-[#FB6F92] text-lg my-2';
    return (
        <footer className=" bg-white  border-t-2 border-solid shadow-sm static">
            <div className='container mx-auto'>
                <img className='w-auto h-auto' src="/Images/LogoTeam.png" />
                <ul className='mx-10 pb-10'>
                    <li className={liClass}>Lô E2 Khu Công nghệ cao, TP. Thủ Đức</li>
                    <li className={liClass}>Số điện thoại: (028) 3666 9555</li>
                    <li className={liClass}>Email: stdhospital@gmail.com</li>
                    <li className={liClass}>Website: bvsinhtodau.vn</li>
                </ul>
            </div>
        </footer>
    );
};

export default Footer;