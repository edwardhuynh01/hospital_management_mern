import React from 'react';
import Title from '../../../components/Title/Title';
import { IoChevronForwardOutline } from "react-icons/io5";

const CustomerGuidePage = () => {
    return (
        <div className='bg-white'>
            <div className='container mx-auto'>
            <Title>QUY TRÌNH KHÁM BỆNH</Title>
                <ul className='pt-12 pb-24 mx-60 <=md:mx-auto flex flex-col'>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 p-2 flex-shrink-0'>
                            <p>BƯỚC 1</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>ĐẶT LỊCH KHÁM</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Đăng nhập phần mềm trên web.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Chọn hình thức đặt khám: Theo ngày hoặc theo bác sĩ.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Nhập thông tin bệnh nhân: Bằng số hồ sơ hoặc tạo mới.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Chọn thông tin khám: Chuyên khoa, bác sĩ, ngày khám, giờ khám.</li>
                            </ul>
                        </div>
                    </li>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 py-2 pr-1 pl-2 flex-shrink-0'>
                            <p>BƯỚC 2</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>THANH TOÁN TIỀN KHÁM</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Chọn loại thẻ thanh toán: Thẻ khám bệnh của bệnh viện Sinh Tố Dâu, thẻ thanh toán quốc tế hoặc thẻ ATM nội địa.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Kiểm tra tiền khám, các loại phí và tổng tiền thanh toán.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Nhập thông tin thẻ để tiến hành thanh toán.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 font-semibold cursor-default'>Lưu ý:</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Thanh toán bằng thẻ khám bệnh của Bệnh viện Sinh Tố Dâu, phí tiện ích sẽ có mức thấp nhất.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Đối với thẻ khám Bệnh viện Sinh Tố Dâu, vui lòng đăng ký chức năng thanh toán trực tuyến tại các chi nhánh Vietinbank trong cả nước, trước khi sử dụng.</li>
                            </ul>
                        </div>
                    </li>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 py-2 pr-[3px] pl-2 flex-shrink-0'>
                            <p>BƯỚC 3</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>XÁC NHẬN NGƯỜI BỆNH ĐẾN BỆNH VIỆN KHÁM THEO HẸN</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Sau khi đặt khám thành công phiếu khám điện tử sẽ được gửi ngay qua email, tin nhắn sms và trên phần mềm.</li>
                                <li className='flex flex-col justify-items-start text-md leading-7 hover:text-[#ff8fab] cursor-default'>
                                    <p className='flex items-baseline justify-start gap-2'><IoChevronForwardOutline />Đến ngày khám,</p>
                                    <ul className='pl-6 pt-4'>
                                        <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Người bệnh sẽ đến trực tiếp phòng khám trước giờ hẹn 15-30 phút để khám bệnh .</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 py-2 pr-[2px] pl-2 flex-shrink-0'>
                            <p>BƯỚC 4</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>KHÁM VÀ THỰC HIỆN CẬN LÂM SÀNG</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Người bệnh khám tại các phòng khám chuyên khoa theo sự hướng dẫn của nhân viên y tế.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Thực hiện cận lâm sàng (nếu có) và đóng phí tại các quầy thu ngân</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Khi đủ kết quả cận lâm sàng, người bệnh quay lại phòng khám gặp Bác sĩ nhận toa thuốc.</li>
                            </ul>
                        </div>
                    </li>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 py-2 pr-[2px] pl-2 flex-shrink-0'>
                            <p>BƯỚC 5</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>NHẬN THUỐC</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Thực hiện kết toán tại các quầy thu ngân và nhận thuốc tại quầy thuốc.</li>
                            </ul>
                        </div>
                    </li>
                    <li className='flex <=md:flex-col w-full'>
                        <div className='text-[#fb6f92] text-xl font-semibold hover:text-[#ff8fab] mr-8 py-2 pr-[2px] pl-2 flex-shrink-0'>
                            <p>BƯỚC 6</p>
                        </div>
                        <div className='flex flex-col items-center <=md:hidden border-b-[2px] border-solid border-b-slate-200 flex-shrink-0'>
                            <div className='w-[2px] h-6 bg-slate-200'></div>
                            <div className='h-4 w-4 bg-[#ff8fab] rounded-full'></div>
                            <div className='w-[2px] h-full bg-slate-200'></div>
                        </div>
                        <div className='px-4 border-b-[2px] border-solid border-b-slate-200 py-4 flex-1 flex flex-col'>
                            <div className='text-[#fb6f92] text-xl font-semibold'><h2>ĐẶT LỊCH TÁI KHÁM</h2></div>
                            <ul className='p-4'>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Sử dụng phần mềm đặt hẹn tái khám như BƯỚC 1 và BƯỚC 2 để nhận phiếu khám điện tử.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 font-semibold cursor-default'>Lưu ý:</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Chọn bác sĩ khám và ngày tái khám theo thông tin trên toa thuốc.</li>
                                <li className='flex items-baseline justify-start gap-2 text-md leading-7 hover:text-[#ff8fab] cursor-default'><IoChevronForwardOutline />Nhập thông tin người bệnh bằng số hồ sơ trên toa thuốc (không được tạo mới).</li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default CustomerGuidePage;
