import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
  IoArrowBackCircle,
  IoBusinessOutline,
  IoFitnessOutline,
  IoPersonOutline,
  IoDocumentTextOutline,
} from "react-icons/io5";
import MedicalScheduleCalendar from "../../../components/Appointment/MedicalScheduleCalendar";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import doctorApi from "../../../api/doctorApi";
import specialistApi from "../../../api/specialistApi";
const Back = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
const Bottom = styled.div`
  border-top: 3px solid #dee2e6;
`;
const Side = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
function ChooseMedicalSchedule(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  useEffect(() => {
    if (!user) {
      navigated("/login");
    }
  });
  const doctorId = useSelector(
    (state) => state.appointment?.appointment?.doctorId
  );
  const encode = (id) => {
    return btoa(id);
  };
  const [docter, setDocter] = useState([]);
  const [specialist, setSpecialist] = useState();
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchDocter = async () => {
        const data = await doctorApi.getDoctorById(
          axiosJWT,
          navigated,
          accessToken,
          doctorId
        );
        setDocter(data.data);
      };
      fetchDocter();
    } catch (error) {}
  }, []);
  useEffect(() => {
    const fetchSpec = async () => {
      try {
        const response = await specialistApi.getSpecialistById(
          encode(docter.specialist)
        );

        setSpecialist(response);
      } catch (error) {
        console.error("Error fetching spec:", error);
      }
    };
    fetchSpec();
  }, [docter]);
  return (
    <div className="w-full flex justify-center py-5 bg-white">
      <div className="w-4/5 lg:flex justify-between ">
        <div className="w-full max-h-96 lg:w-1/5 flex flex-wrap lg:mr-4 mb-4">
          <div className="w-full bg-[#f26a8d] text-white px-5 py-3 rounded-t-md font-semibold">
            <h2>Thông tin khám</h2>
          </div>
          <Side className="w-full pr-5 pl-4 pt-3 pb-10">
            <div className="flex justify-start items-start my-2">
              <div className="mx-2 pt-1">
                <IoBusinessOutline></IoBusinessOutline>
              </div>
              <p className="font-normal">Bệnh viện Đa Khoa Sinh Tố Dâu</p>
            </div>
            <div className="flex justify-start items-start my-2">
              <div className="mx-2 pt-1">
                <IoFitnessOutline></IoFitnessOutline>
              </div>
              <p className="font-normal">
                Chuyên khoa: {specialist?.specialistName}
              </p>
            </div>
            <div className="flex justify-start items-start my-2">
              <div className="mx-2 pt-1">
                <IoPersonOutline></IoPersonOutline>
              </div>
              <p className="font-normal">Bác sĩ: {docter?.name}</p>
            </div>
            <div className="flex justify-start items-start my-2">
              <div className="mx-2 pt-1">
                <IoDocumentTextOutline></IoDocumentTextOutline>
              </div>
              <p className="font-normal">Dịch vụ: Khám dịch vụ</p>
            </div>
          </Side>
        </div>
        <div className="w-full lg:w-4/5 lg:ml-4">
          <MedicalScheduleCalendar
            dateOfWeek={docter?.dateOfWeek}
            day={docter?.days}
          ></MedicalScheduleCalendar>
          <Bottom className="w-full py-5">
            <Back
              className="w-32 h-11 flex items-center justify-center mx-2 pr-2"
              //   onClick={HandleRewriteClick}
              href="/ChooseDoctor"
            >
              <div className="flex justify-center items-center">
                <div className="mx-2 ">
                  <IoArrowBackCircle></IoArrowBackCircle>
                </div>
                Quay lại
              </div>
            </Back>
          </Bottom>
        </div>
      </div>
    </div>
  );
}

export default ChooseMedicalSchedule;
