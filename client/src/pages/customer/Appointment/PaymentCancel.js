import React, { useEffect } from "react";
import { clearAppointment } from "../../../redux/appointmentSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

function PaymentCancel(props) {
  const DISPATCH = useDispatch();
  DISPATCH(clearAppointment());
  const user = useSelector((state) => state.auth.login?.currentUser);
  const navigate = useNavigate();
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  });
  return (
    <div className="w-full flex justify-center">
      <div className="w-11/12 h-16 px-5 pt-5 py-4 text-center font-semibold rounded-md my-5 bg-[#ffc2d1] text-[#fb6f92]">
        Hủy thanh toán thành công
      </div>
    </div>
  );
}

export default PaymentCancel;
