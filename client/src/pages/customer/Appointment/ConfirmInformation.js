import React, { useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import {
  IoPersonCircleOutline,
  IoPhonePortraitOutline,
  IoNavigateCircleOutline,
  IoArrowBackCircle,
  IoArrowForwardOutline,
} from "react-icons/io5";
import PatientInformation from "../../../components/Appointment/PatientInformation";
import ConfirmMedicalExaminationInformation from "../../../components/Appointment/ConfirmMedicalExaminationInformation";
import { useDispatch, useSelector } from "react-redux";
import recordApi from "../../../api/recordApi";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
const bounce = keyframes`
  0% {
    opacity: 0;
    transform: translateX(-100%);
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    transform: translateX(100%);
  }
`;
const AnimatedArrow = styled(IoArrowForwardOutline)`
  animation: ${bounce} 2s linear infinite;
`;
const Confirm = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: #fb6f92;
  }
`;
const Back = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
const Bottom = styled.div`
  border-top: 3px solid #dee2e6;
`;
const Side = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
function ConfirmInformation(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [record, setRecord] = useState();
  const recordId = useSelector(
    (state) => state.appointment?.appointment?.recordId
  );
  const encode = (id) => {
    return btoa(id);
  };
  useEffect(() => {
    if (!user) {
      navigated("/login");
    }
  });
  useEffect(() => {
    const fetchRecord = async () => {
      try {
        const data = await recordApi.getRecordById(
          axiosJWT,
          navigated,
          accessToken,
          encode(recordId)
        );
        setRecord(data.data);
      } catch (error) {
        throw error;
      }
    };
    fetchRecord();
  }, []);
  return (
    <div className="w-full flex justify-center py-5 bg-white">
      <div className="w-full md:w-4/5 lg:flex justify-between ">
        <div className="w-full h-56 lg:w-1/5 flex flex-wrap lg:mr-4 mb-4">
          <div className="w-full bg-[#f26a8d] text-white px-5 py-3 rounded-t-md font-semibold">
            <h2>Thông tin bệnh nhân</h2>
          </div>
          <Side className="w-full pr-5 pl-4 pt-3 pb-7">
            <div className="flex justify-start items-start mb-4 mt-2">
              <div className="mx-2 mt-1">
                <IoPersonCircleOutline></IoPersonCircleOutline>
              </div>
              <p className="font-semibold text-sm">{record?.fullName}</p>
            </div>
            <div className="flex justify-start items-start my-4">
              <div className="mx-2 mt-1">
                <IoPhonePortraitOutline></IoPhonePortraitOutline>
              </div>
              <p className="font-normal text-sm">{record?.phoneNumber}</p>
            </div>
            <div className="flex justify-start items-start my-4">
              <div className="mx-2 mt-1">
                <IoNavigateCircleOutline></IoNavigateCircleOutline>
              </div>
              <p className="font-normal text-sm">{record?.address}</p>
            </div>
          </Side>
        </div>
        <div className="w-full lg:w-4/5 lg:ml-4">
          <ConfirmMedicalExaminationInformation />
          <PatientInformation></PatientInformation>
          <Bottom className="flex justify-between w-full py-5">
            <Back
              className="w-32 h-11 flex items-center justify-center mx-2 pr-2"
              //   onClick={HandleRewriteClick}
              href="/ChooseMedicalSchedule"
            >
              <div className="flex justify-center items-center">
                <div className="mx-2 ">
                  <IoArrowBackCircle></IoArrowBackCircle>
                </div>
                Quay lại
              </div>
            </Back>
            <div className="flex 1/2 lg:w-1/3">
              <Confirm
                className="w-40 h-11 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white"
                //   onClick={handleSubmitClick}
                href="/ChooseSpecialist"
              >
                Thêm chuyên khoa
              </Confirm>
              <Confirm
                className="w-32 h-11 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white pl-2"
                //   onClick={handleSubmitClick}
                href="/payment"
              >
                <div className="flex justify-center items-center">
                  Xác nhận
                  <div className="mx-2 ">
                    <AnimatedArrow size={16} />
                  </div>
                </div>
              </Confirm>
            </div>
          </Bottom>
        </div>
      </div>
    </div>
  );
}

export default ConfirmInformation;
