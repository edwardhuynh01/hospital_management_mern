import React, { useEffect, useReducer } from "react";
import styled from "styled-components";
import { IoArrowBackCircle, IoPersonAdd } from "react-icons/io5";
import RecordsList from "../../../components/Appointment/RecordsList";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const Header2 = styled.h2`
  border-bottom: 5px solid rgba(255, 202, 212, 0.4);
  height: 28px;
`;
const Bottom = styled.div`
  border-top: 3px solid #ced4da;
`;
const Back = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
const AddNew = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: #fb6f92;
  }
`;
function ChooseRecord(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const navigate = useNavigate();
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  });
  return (
    <div className="w-full flex items-center justify-center bg-white">
      <div className="w-11/12 lg:w-7/12 my-5">
        <div className="w-full flex flex-wrap justify-center items-center">
          <div className="w-full flex justify-center items-center text-center font-semibold text-xl uppercase my-5">
            <Header2>Chọn hồ sơ bệnh nhân</Header2>
          </div>
          <div className="my-4 w-full flex flex-wrap justify-center">
            <RecordsList></RecordsList>
          </div>
          <Bottom className="w-full lg:w-1/2 mt-6 flex justify-between items-center py-5 px-1">
            <Back
              className="w-32 h-11 flex items-center justify-center mx-2 pr-2"
              //   onClick={HandleRewriteClick}
              href="/"
            >
              <div className="flex justify-center items-center">
                <div className="mx-2 ">
                  <IoArrowBackCircle></IoArrowBackCircle>
                </div>
                Quay lại
              </div>
            </Back>
            <AddNew
              className="w-32 h-11 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white pr-2"
              //   onClick={handleSubmitClick}
              href="/profile/MakeMedicalRC"
            >
              <div className="flex justify-center items-center">
                <div className="mx-2 ">
                  <IoPersonAdd></IoPersonAdd>
                </div>
                Thêm hồ sơ
              </div>
            </AddNew>
          </Bottom>
        </div>
      </div>
    </div>
  );
}

export default ChooseRecord;
