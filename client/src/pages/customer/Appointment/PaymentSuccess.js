import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { successPayment } from "../../../redux/apiRequest";
import { useLocation, useNavigate } from "react-router-dom";

function PaymentSuccess(props) {
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const id = query.get("id");
  const paymentToken = useSelector((state) => state.appointment?.paymentToken);
  const [msg, setMsg] = useState("");
  const DISPATCH = useDispatch();
  const user = useSelector((state) => state.auth.login?.currentUser);
  const navigate = useNavigate();
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  });
  useEffect(() => {
    try {
      if (!paymentToken || typeof paymentToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchPaymentSuccess = async () => {
        const data = await successPayment(paymentToken, id, DISPATCH);
        setMsg(data.data);
      };
      fetchPaymentSuccess();
    } catch (error) {
      setMsg(error.data);
    }
  }, []);
  return (
    <div className="w-full flex justify-center">
      <div className="w-11/12 h-16 px-5 pt-5 py-4 text-center font-semibold rounded-md my-5 bg-[#b7e4c7] text-[#22577a]">
        {msg}
      </div>
    </div>
  );
}

export default PaymentSuccess;
