import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import CountUp from 'react-countup';
import LoadingEdit from '../../components/Loading/LoadingEdit.js';
import accountApi from "../../api/accountApi";
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import EditAccountForm from "../../components/AdminAddPopup/EditAccountForm";
const AccountsPage = () => {
    const user = useSelector((state) => state.auth.login?.currentUser);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    let accessToken = user?.accessToken;
    let axiosJWT = createAxios(user, dispatch, loginSuccess);
    const navigated = useNavigate();
    const [confirmedSearch, setConfirmedSearch] = useState("");
    const [search, setSearch] = useState("");
    const [accountList, setAccountList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isDeleted, setIsDeleted] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [id, setId] = useState();

    const handleFilter = () => {
        setConfirmedSearch(search);
        setSearch('');
    };

    const handleEditClick = (accountId) => {
        setId(accountId);
        setIsModalEditOpen(true);
    };

    useEffect(() => {
        setIsLoading(true);
        const fetchData = async () => {
            try {
                if (!user) {
                    navigate("/login");
                } else if (user.role !== "admin") {
                    navigate("/");
                } else {
                    if (!accessToken || typeof accessToken !== "string") {
                        throw new Error("Token không hợp lệ");
                    }
                    const accountData = await accountApi.getAllAccount(axiosJWT, navigate, accessToken);
                    setAccountList(accountData.data || []);
                }
            } catch (error) {
                console.error(error);
            } finally {
                setIsLoading(false);
            }
        };
        fetchData();
    }, [isDeleted]);

    const handleDelete = (id) => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <div className="bg-slate-100 p-6 rounded-lg shadow-xl">
                    <h1 className="text-2xl text-[#fb6f92] font-semibold mb-4 text-start h-full w-full">
                        Xác nhận xóa
                    </h1>
                    <p className="mb-4">Bạn có chắc chắn muốn xóa tài khoản này không ?</p>
                    <div className="flex justify-center gap-4">
                        <button
                            onClick={async () => {
                                try {
                                    const deleteData = await accountApi.deleteAccount(
                                        axiosJWT,
                                        navigated,
                                        accessToken,
                                        id
                                    );
                                    if (deleteData) {
                                        setIsDeleted(!isDeleted);
                                        // if (deleteData?.data?.avatarImage) {
                                        //     deleteFile(deleteData.data.avatarImage);
                                        // }
                                        toast("Xóa tài khoản thành công!");
                                    } else {
                                        toast.error("Đã xảy ra lỗi khi xóa tài khoản.");
                                    }
                                } catch (error) {
                                    toast.error("Đã xảy ra lỗi khi xóa tài khoản.");
                                    console.error("Error deleting record:", error);
                                }
                                onClose();
                            }}
                            className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
                        >
                            Có
                        </button>
                        <button
                            onClick={onClose}
                            className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
                        >
                            Không
                        </button>
                    </div>
                </div>
            ),
        });
    };

    const handleEditAccount = async (accountData) => {
        try {
            const updatedAccount = await accountApi.updateAccount(
                axiosJWT,
                navigated,
                accessToken,
                id,
                accountData
            );
            if (updatedAccount) {
                setIsDeleted(!isDeleted);
                setIsModalEditOpen(false);
                toast("Cập nhật tài khoản thành công!");
            } else {
                toast.error("Đã xảy ra lỗi khi cập nhật tài khoản.");
            }
        } catch (error) {
            toast.error("Đã xảy ra lỗi khi cập nhật tài khoản.");
            console.error("Error adding account:", error);
        }
    };

    return (
        <main className="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
            <div className="p-6 h-full">
                <div className="grid grid-cols-1">
                    <div className="bg-white border border-gray-100 shadow-md shadow-black/4 p-9 rounded-md">
                        <div className="flex justify-between mb-4 items-start">
                            <div className="font-medium text-gray-500">Số Tài Khoản: <CountUp end={accountList.length} duration={2} /></div>
                        </div>
                        <div className="flex items-center mb-4 order-tab gap-4">
                            <button type="button" data-tab="order" data-tab-page="active" className="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-10 rounded-tl-md rounded-bl-md rounded-tr-md rounded-br-md hover:bg-sky-500 active">Thêm</button>
                            <input type="text" className="shadow-md shadow-black/4 ml-3 py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500" placeholder="Tìm kiếm..." value={search} onChange={(e) => setSearch(e.target.value)} />
                            <button type="button" data-tab="order" data-tab-page="active" className="shadow-md shadow-black/4 bg-blue-400 text-sm font-medium text-white py-2 px-6 rounded-tl-md rounded-bl-md rounded-tr-md rounded-br-md hover:bg-blue-500 active" onClick={handleFilter}>Lọc</button>
                        </div>
                        <div className="overflow-x-auto">
                            <div className="overflow-y-auto max-h-[508px]">
                                <table className="w-full min-w-[800px]" data-tab-for="order" data-page="active">
                                    <thead>
                                        <tr>
                                            <th style={{ minWidth: '220px', tableLayout: 'fixed' }} className="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left rounded-tl-md rounded-bl-md">Người sở hữu</th>
                                            <th style={{ minWidth: '250px', tableLayout: 'fixed' }} className="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-center">Tên đăng nhập</th>
                                            <th style={{ minWidth: '180px', tableLayout: 'fixed' }} className="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-center">Vị trí</th>
                                            <th style={{ minWidth: '150px', tableLayout: 'fixed' }} className="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-center">Số Điện Thoại</th>
                                            <th style={{ minWidth: '200px', tableLayout: 'fixed' }} className="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-5 bg-gray-100 text-center rounded-tr-md rounded-br-md">Chức Năng</th>
                                        </tr>
                                    </thead>
                                    {
                                        isLoading ? (
                                            <LoadingEdit />
                                        ) : (
                                            <tbody>
                                                {accountList.filter((account) => {
                                                    return confirmedSearch.toLowerCase() === ''
                                                        ? true
                                                        : account.username.toLowerCase().includes(confirmedSearch.toLowerCase());
                                                }).map((account) => (
                                                    <tr key={account._id}>
                                                        <td className="py-2 px-1 border-b border-b-gray-50 overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                            <div className="flex items-left">
                                                                {/* <img src={`data:image/png;base64,${account.avatarImage}`} alt={account.username} className="w-8 h-8 rounded object-cover block" /> */}
                                                                <span href="#" className="text-gray-600 text-sm font-medium hover:text-blue-500 ml-2 truncate text-left">{account.username}</span>
                                                            </div>
                                                        </td>
                                                        <td className="py-2 px-4 border-b border-b-gray-50 overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                            <span className="text-[13px] font-medium text-gray-400">{account.email}</span>
                                                        </td>
                                                        <td className="py-2 px-3 border-b border-b-gray-50 overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                            <span className={account.role === 'customer' ?
                                                                "inline-block p-1 rounded bg-pink-500/10 text-pink-500 font-medium text-[12px] leading-none" :
                                                                account.role === 'admin' ? "inline-block p-1 rounded bg-blue-500/10 text-blue-500 font-medium text-[12px] leading-none" :
                                                                    "inline-block p-1 rounded bg-orange-500/10 text-orange-500 font-medium text-[12px] leading-none"
                                                            }>{account.role}</span>
                                                        </td>
                                                        <td className="py-2 px-3 border-b border-b-gray-50 overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                            <span className="text-[13px] font-medium text-gray-400">{account.phoneNumber}</span>
                                                        </td>
                                                        <td className="py-2 px-3 border-b border-b-gray-50 overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                            <div className="flex items-center gap-1 justify-center">
                                                                <div className="hover:bg-pink-100 rounded">
                                                                    <button
                                                                        type="button"
                                                                        class="px-2 mt-1"
                                                                        onClick={() => handleEditClick(account._id)}
                                                                    >
                                                                        <box-icon name="edit-alt"></box-icon>
                                                                    </button>
                                                                </div>
                                                                <div className="hidden sm:block mx-2 lg:mx-px w-px h-5 bg-gray-200 dark:bg-gray-900"></div>
                                                                <div className="hover:bg-pink-100 rounded">
                                                                    <button type="button" className="px-2 mt-1" onClick={() => handleDelete(account?._id)}>
                                                                        <box-icon name='trash'></box-icon>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        )
                                    }
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <EditAccountForm
                    isOpen={isModalEditOpen}
                    onRequestClose={() => setIsModalEditOpen(false)}
                    onSubmit={handleEditAccount}
                    isId={id}
                />
            </div>
        </main>
    );
};

export default AccountsPage;