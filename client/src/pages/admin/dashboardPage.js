import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import doctorApi from "../../api/doctorApi";
import specialistApi from "../../api/specialistApi";
import appointmentApi from "../../api/appointmentApi";
import accountApi from "../../api/accountApi";
import recordApi from "../../api/recordApi";
import servicesApi from "../../api/servicesApi";
import CountUp from "react-countup";

const DashboardPage = () => {
  const [isDeleted, setIsDeleted] = useState(false);
  const navigate = useNavigate();
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [docterList, setDocterList] = useState([]);
  const [specialists, setSpecialists] = useState([]);
  const [id, setId] = useState();
  const [appointments, setAppointments] = useState([]);
  const [account, setAccount] = useState([]);
  const [records, setRecords] = useState([]);
  const [specialist, setSpecialist] = useState([]);
  const [doctor, setDoctor] = useState([]);
  const [serviceList, setServiceList] = useState();
  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {
          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchService = async () => {
            const data = await servicesApi.getAllService();
            setServiceList(data);
          };
          fetchService();
        }
      }
    } catch (error) {}
  }, [isDeleted]);

  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {
          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchAppointment = async () => {
            const data = await appointmentApi.getAllAppointment(
              axiosJWT,
              navigated,
              accessToken
            );
            setAppointments(data.data);
          };
          fetchAppointment();
          const fetchAccount = async () => {
            const data = await accountApi.getAllAccount(
              axiosJWT,
              navigated,
              accessToken
            );
            setAccount(data.data);
          };
          fetchAccount();
          const fetchRecord = async () => {
            const data = await recordApi.getAllRecord(
              axiosJWT,
              navigated,
              accessToken
            );
            setRecords(data.data);
          };
          fetchRecord();
          const fetchSpec = async () => {
            const data = await specialistApi.getAllSpecialist();
            setSpecialist(data);
          };
          fetchSpec();
          const fetchDoctor = async () => {
            const data = await doctorApi.getAllDoctor(
              axiosJWT,
              navigated,
              accessToken
            );
            setDoctor(data.data);
          };
          fetchDoctor();
        }
      }
    } catch (error) {}
  }, []);

    useEffect(() => {
        try {
            if (!user) {
                navigated("/login");
            } else {
                if (user.role !== "admin") {
                    navigated("/");
                } else {

                    if (!accessToken || typeof accessToken !== "string") {
                        throw new Error("Token không hợp lệ");
                    }
                    const fetchAppointment = async () => {
                        const data = await appointmentApi.getAllAppointment(
                            axiosJWT,
                            navigated,
                            accessToken
                        );
                        setAppointments(data.data);
                    };
                    fetchAppointment();
                    const fetchAccount = async () => {
                        const data = await accountApi.getAllAccount(
                            axiosJWT,
                            navigated,
                            accessToken
                        );
                        setAccount(data.data);
                    };
                    fetchAccount();
                    const fetchRecord = async () => {
                        const data = await recordApi.getAllRecord(
                            axiosJWT,
                            navigated,
                            accessToken
                        );
                        setRecords(data.data);
                    }
                    fetchRecord();
                    const fetchSpec = async () => {
                        const data = await specialistApi.getAllSpecialist();
                        setSpecialist(data);
                    }
                    fetchSpec();
                    const fetchDoctor = async () => {
                        const data = await doctorApi.getAllDoctor(
                            axiosJWT,
                            navigated,
                            accessToken
                        );
                        setDoctor(data.data);
                    }
                    fetchDoctor();
                }
            }
        } catch (error) { }
    }, []);

    useEffect(() => {
        try {
            if (!user) {
                navigated("/login");
            } else {
                if (user.role !== "admin") {
                    navigated("/");
                } else {

                    if (!accessToken || typeof accessToken !== "string") {
                        throw new Error("Token không hợp lệ");
                    }
                    const fetchDocter = async () => {
                        try {
                            const data = await doctorApi.getAllDoctor(
                                axiosJWT,
                                navigated,
                                accessToken
                            );
                            setDocterList(data.data);
                        } catch (error) {

                        }
                    };
                    fetchDocter();
                    const fetchSpec = async () => {
                        try {
                            const data = await specialistApi.getAllSpecialist();
                            setSpecialists(data);
                        } catch (error) {

                        }
                    }
                    fetchSpec();
                }
            }
        } catch (error) { }
    }, [isDeleted]);
    return (
        <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
            <div className="p-6">
                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 mb-6">
                    <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
                        <div class="flex justify-between mb-6">
                            <div>
                                <div class="text-2xl font-semibold mb-1"><span className="text-2xl font-semibold"><CountUp end={docterList?.length} duration={3} className="text-2xl font-semibold" /></span></div>
                                <div class="text-sm font-medium text-gray-400">Số Lượng Bác Sĩ</div>
                            </div>
                            {/* <div class="dropdown">8
                                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600">dsda<i class="ri-more-fill"></i></button>
                                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                                    </li>
                                </ul>
                            </div> */}
            </div>
            <Link
              to="/admin/doctor"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
          <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between mb-6">
              <div>
                <div class="flex items-center mb-1">
                  <div class="text-2xl font-semibold">
                    <span className="text-2xl font-semibold">
                      <CountUp
                        end={serviceList?.length}
                        duration={3}
                        className="text-2xl font-semibold"
                      />
                    </span>
                  </div>
                  {/* <div class="p-1 rounded bg-emerald-500/10 text-emerald-500 text-[12px] font-semibold leading-none ml-2">+30%</div> */}
                </div>
                <div class="text-sm font-medium text-gray-400">
                  Số Lượng Dịch Vụ
                </div>
              </div>
              {/* <div class="dropdown">
                                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600"><i class="ri-more-fill"></i></button>
                                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                                    </li>
                                </ul>
                            </div> */}
            </div>
            <Link
              to="/admin/service"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
          <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between mb-6">
              <div>
                <div class="text-2xl font-semibold mb-1">
                  <span class="text-base font-normal text-gray-400 align-top"></span>
                  <span className="text-2xl font-semibold">
                    <CountUp
                      end={appointments?.length}
                      duration={3}
                      className="text-2xl font-semibold"
                    />
                  </span>
                </div>
                <div class="text-sm font-medium text-gray-400">
                  Số Lịch Đặt Khám
                </div>
              </div>
              {/* <div class="dropdown">
                                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600">dsd<i class="ri-more-fill"></i></button>
                                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                                    </li>
                                </ul>
                            </div> */}
            </div>
            <Link
              to="/admin/appointment"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
};

export default DashboardPage;