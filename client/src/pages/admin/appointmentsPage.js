import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import appointmentApi from "../../api/appointmentApi";
import accountApi from "../../api/accountApi";
import recordApi from "../../api/recordApi";
import reportApi from "../../api/reportApi";
import specialistApi from "../../api/specialistApi";
import doctorApi from "../../api/doctorApi";
import CountUp from "react-countup";
import ModalAccountDetails from "../../components/AdminAddPopup/ModalAccountDetails";

const AppointmentsPage = () => {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [appointments, setAppointments] = useState([]);
  const [account, setAccount] = useState([]);
  const [records, setRecords] = useState([]);
  const [reports, setReports] = useState([]);
  const [specialist, setSpecialist] = useState([]);
  const [doctor, setDoctor] = useState([]);
  const [search, setSearch] = useState("");
  const [confirmedSearch, setConfirmedSearch] = useState("");
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const handleFilter = () => {
    setConfirmedSearch(search);
    setSearch("");
  };
  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {
          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchReport = async () => {
            const data = await reportApi.getAllReport(
              axiosJWT,
              navigated,
              accessToken
            );
            setReports(data.data);
          };
          fetchReport();
          const fetchAppointment = async () => {
            const data = await appointmentApi.getAllAppointment(
              axiosJWT,
              navigated,
              accessToken
            );
            setAppointments(data.data);
          };
          fetchAppointment();
          const fetchAccount = async () => {
            const data = await accountApi.getAllAccount(
              axiosJWT,
              navigated,
              accessToken
            );
            setAccount(data.data);
          };
          fetchAccount();
          const fetchRecord = async () => {
            const data = await recordApi.getAllRecord(
              axiosJWT,
              navigated,
              accessToken
            );
            setRecords(data.data);
          };
          fetchRecord();
          const fetchSpec = async () => {
            const data = await specialistApi.getAllSpecialist();
            setSpecialist(data);
          };
          fetchSpec();
          const fetchDoctor = async () => {
            const data = await doctorApi.getAllDoctor(
              axiosJWT,
              navigated,
              accessToken
            );
            setDoctor(data.data);
          };
          fetchDoctor();
        }
      }
    } catch (error) {}
  }, []);
  const formatDate = (date) => {
    if (!date) return "Invalid Date"; // Trả về chuỗi thông báo nếu không có giá trị ngày
    const dateObj = new Date(date);
    if (isNaN(dateObj.getTime())) return "Invalid Date"; // Kiểm tra xem dateObj có hợp lệ không
    return dateObj.toISOString().split("T")[0];
  };
  const handleAccountClick = (accountId, recordId, doctorId, appItem) => {
    const accountData = account.find((x) => x._id === accountId);
    const recordData = records.find((x) => x._id === recordId);
    const doctorData = doctor.find((x) => x._id === doctorId);
    const combinedData = {
      ...accountData,
      fullName: recordData?.fullName,
      IdentityCardNumber: recordData?.IdentityCardNumber,
      sex: recordData?.sex,
      date: recordData?.date,
      address: recordData?.address,
      doctorName: doctorData?.name,
      appointmentDay: appItem?.day + "-" + appItem?.month + "-" + appItem?.year,
      appointmentTime: appItem?.timeStart + "-" + appItem?.timeEnd,
      price:
        specialist
          ?.find((x) => x._id === appItem.specialistId)
          ?.price.toLocaleString("vi-VN") + " VNĐ",
      isPayment: reports.find((x) => x.appointment[0] === appItem._id)
        ?.isPayment
        ? "Đã thanh toán"
        : "Chưa thanh toán",
    };

    setSelectedAccount(combinedData);
    setShowModal(true);
  };

  return (
    <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
      <div className="p-6 h-full">
        <div class="grid grid-cols-1">
          <div class="bg-white border border-gray-100 shadow-md shadow-black/4 p-9 rounded-md ">
            <div class="flex justify-between mb-4 items-start">
              <div class="font-medium text-gray-500">
                Số Lượng Đặt Lịch Khám:{" "}
                <CountUp end={appointments?.length} duration={2} />
              </div>
            </div>
            <div class="flex items-center mb-4 order-tab gap-4">
              {/* <button type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-10 rounded-tl-md rounded-bl-md rounded-tr-md rounded-br-md hover:bg-sky-500 active">Thêm</button> */}
              {/* <button type="button" data-tab="order" data-tab-page="completed" class=" bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 hover:text-gray-600">Completed</button>
                            <button type="button" data-tab="order" data-tab-page="canceled" class="bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 rounded-tr-md rounded-br-md hover:text-gray-600">Canceled</button> */}
              <input
                type="text"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                class="shadow-md shadow-black/4 ml-3 py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500"
                placeholder="Tìm kiếm..."
              ></input>
              <button
                type="button"
                onClick={handleFilter}
                data-tab="order"
                data-tab-page="active"
                class="shadow-md shadow-black/4 bg-blue-400 text-sm font-medium text-white py-2 px-6 rounded-tl-md rounded-bl-md rounded-tr-md  rounded-br-md hover:bg-blue-500 active"
              >
                Lọc
              </button>
            </div>
            <div class="overflow-x-auto">
              <div class="overflow-y-auto max-h-[508px]">
                <table
                  class="w-full min-w-[800px] "
                  data-tab-for="order"
                  data-page="active"
                >
                  <thead>
                    <tr>
                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left rounded-tl-md rounded-bl-md"
                      >
                        Tên Tài Khoản
                      </th>
                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Tên Bệnh Nhân
                      </th>
                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Giá khám
                      </th>

                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Thanh toán
                      </th>

                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Ngày Đặt Khám
                      </th>
                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Thời Gian Khám
                      </th>
                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Chuyên Khoa
                      </th>

                      <th
                        style={{ minWidth: "250px", tableLayout: "fixed" }}
                        class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left"
                      >
                        Chỉ Định Bác Sĩ
                      </th>
                      {/* <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-5 bg-gray-100 text-left rounded-tr-md rounded-br-md">Chức Năng</th> */}
                    </tr>
                  </thead>
                  <tbody>
                    {appointments
                      ?.filter((appItem) => {
                        return confirmedSearch.toLowerCase() === ""
                          ? true
                          : account
                              ?.find((x) => x._id === appItem.id)
                              ?.username.toLowerCase()
                              .includes(confirmedSearch.toLowerCase());
                      })
                      .map((appItem) => (
                        <>
                          <tr
                            key={appItem?._id}
                            onClick={() =>
                              handleAccountClick(
                                appItem.id,
                                appItem.recordId,
                                appItem.doctorId,
                                appItem
                              )
                            }
                            className="cursor-pointer hover:bg-blue-50 transition-all ease-in-out"
                          >
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <div class="flex items-center">
                                <div
                                  key={appItem?._id}
                                  className="text-gray-600 text-sm font-medium ml-2 truncate"
                                >
                                  {
                                    account?.find((x) => x._id === appItem.id)
                                      ?.username
                                  }
                                </div>
                              </div>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {
                                  records?.find(
                                    (x) => x._id === appItem.recordId
                                  )?.fullName
                                }
                              </span>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {specialist
                                  ?.find((x) => x._id === appItem.specialistId)
                                  ?.price.toLocaleString("vi-VN") + " VNĐ"}
                              </span>
                            </td>

                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {reports.find(
                                  (x) => x.appointment[0] === appItem._id
                                )?.isPayment
                                  ? "Đã thanh toán"
                                  : "Chưa thanh toán"}
                              </span>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {appItem?.day +
                                  "-" +
                                  appItem?.month +
                                  "-" +
                                  appItem?.year}
                              </span>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {appItem?.timeStart + "-" + appItem?.timeEnd}
                              </span>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {
                                  specialist.find(
                                    (x) => x._id === appItem.specialistId
                                  )?.specialistName
                                }
                              </span>
                            </td>
                            <td
                              class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto"
                              style={{
                                maxWidth: "200px",
                                tableLayout: "fixed",
                              }}
                            >
                              <span class="text-[13px] font-medium text-gray-400">
                                {
                                  doctor.find((x) => x._id === appItem.doctorId)
                                    ?.name
                                }
                              </span>
                            </td>
                            {/* 
                                            <td class="py-2 px-4 border-b border-b-gray-50 overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                <div class="flex items-center gap-1 justify-right">
                                                    <div class="hover:bg-pink-100 rounded">
                                                        <button type="button" class="px-2 mt-1">
                                                            <box-icon name='edit-alt' ></box-icon>
                                                        </button>
                                                    </div>
                                                    <div class="hidden sm:block mx-2 lg:mx-px w-px h-5 bg-gray-200 dark:bg-gray-900"></div>
                                                    <div class="hover:bg-pink-100 rounded">
                                                        <button type="button" class="px-2 mt-1">
                                                            <box-icon name='trash' ></box-icon>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td> */}
                          </tr>
                        </>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <ModalAccountDetails
          showModal={showModal}
          setShowModal={setShowModal}
          selectedAccount={selectedAccount}
        />
      </div>
    </main>
  );
};

export default AppointmentsPage;
