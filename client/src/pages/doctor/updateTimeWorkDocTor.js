import React, { useEffect, useReducer, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { createAxios } from '../../createInstance';
import { loginSuccess } from '../../redux/authSlice';
import doctorApi from '../../api/doctorApi';
import Loading from '../../components/Loading/Loading';
import { toast } from 'react-toastify';
const initialState = {
    inputDateOfWeek: "",
};
function reducer(state, action) {
    switch (action.type) {
        case "SET_DATEOFWEEK_VALUE":
            return { ...state, inputDateOfWeek: action.payload };
        default:
            return state;
    }
}
const UpdateTimeWorkDocTor = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const user = useSelector((state) => state.auth.login?.currentUser);
    const DISPATCH = useDispatch();
    const navigated = useNavigate();
    let accessToken = user?.accessToken;
    let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
    const [fileName, setFileName] = useState("Không có tệp nào được chọn");
    const [isLoading, setIsLoading] = useState(false);
    const [docter, setDocter] = useState();
    const [doctorData, setDoctorData] = useState({
        dateOfWeek: "",
        days: [],
    });
    const handleDateOfWeedChange = (event) => {
        handleDateOfWeekSelect(event);
        dispatch({ type: "SET_DATEOFWEEK_VALUE", payload: event.target.value });
        doctorData.dateOfWeek = event.target.value;
    };
    const validateForm = () => {
        let isValid = true;
        if (Number(doctorData.dateOfWeek) === -1) {
            isValid = false;
        }
        return isValid;
    };

    const [checkboxStates, setCheckboxStates] = useState(Array(8).fill(false));
    const [dateOfWeek, setDateOfWeek] = useState(0);
    const [daysOfWeek, setDaysOfWeek] = useState([]);
    const [times, setTimes] = useState([]);
    const dataDay = [];
    daysOfWeek.map((day, index) => dataDay.push({ day, times }));
    const handleSelectTime = (e, index) => {
        const time = e.target.value;
        const timeStart = time.split("-")[0];
        const timeEnd = time.split("-")[1];
        const isChecked = e.target.checked;

        const updatedCheckboxStates = [...checkboxStates];
        updatedCheckboxStates[index] = isChecked;
        setCheckboxStates(updatedCheckboxStates);

        if (isChecked) {
            setTimes([...times, { timeStart, timeEnd }]);
        } else {
            setTimes(times.filter((t) => t.timeStart !== timeStart));
        }
    };
    const handleDateOfWeekSelect = (e) => {
        const selectedDay = e.target.value;
        setDateOfWeek(selectedDay);
        autoCalday(selectedDay);
    };
    const autoCalday = (dayOfWeek) => {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth();
        const currentDay = currentDate.getDate();
        const days = [];
        let date = new Date(currentYear, currentMonth, 1);
        while (date.getMonth() === currentMonth) {
            if (date.getDay() === Number(dayOfWeek) && date.getDate() >= currentDay) {
                days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
            }
            date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
        }
        if (days.length <= 0) {
            while (date.getMonth() === (currentMonth + 1) % 12) {
                if (date.getDay() === Number(dayOfWeek)) {
                    days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
                }
                date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
            }
        }
        setDaysOfWeek(days);
        // console.log(daysOfWeek);
    };
    const encode = (id) => {
        return btoa(id);
    };
    useEffect(() => {
        const fetchDocter = async () => {
            try {
                setTimes([]);
                const response = await doctorApi.getDocterByEmail(
                    axiosJWT,
                    navigated,
                    accessToken,
                    user.email
                );
                setDocter(response.data);
                const dataById = {
                    dateOfWeek: response.data?.dateOfWeek,
                    days: response.data?.days,
                };
                dispatch({
                    type: "SET_DATEOFWEEK_VALUE",
                    payload: response.data?.dateOfWeek,
                });
                setDoctorData(dataById);
                autoCalday(response.data?.dateOfWeek);
                const timesData = response.data?.days[0]?.times?.map((time) => {
                    return {
                        timeStart: time.timeStart,
                        timeEnd: time.timeEnd,
                    };
                });
                const initialCheckboxStates = [
                    "07:00-08:00",
                    "08:00-09:00",
                    "09:00-10:00",
                    "10:00-11:00",
                    "13:00-14:00",
                    "14:00-15:00",
                    "15:00-16:00",
                    "16:00-17:00",
                ].map((timeSlot) =>
                    timesData.some((item) => timeSlot.split("-")[0] === item.timeStart)
                );
                setCheckboxStates(initialCheckboxStates);
                if (timesData.length > 0) {
                    setTimes((prevTimes) => [...prevTimes, ...timesData]);
                }
            } catch (error) {
                console.error("Error fetching news:", error);
            }
        };
        fetchDocter();
        console.log('doctorrr')
    }, []);
    const doctors = {
        dateOfWeek: Number(doctorData?.dateOfWeek),
        days: dataDay,
    };
    const handleEditDoctor = async (newDoctor) => {
        try {
            const response = await doctorApi.updateDoctorByDoctor(axiosJWT, navigated, accessToken, encode(docter?._id), newDoctor);
            toast("Cập nhật bác sĩ thành công!")
        } catch (error) {
            console.error('Failed to update doctor:', error);
            toast.error("Cập nhật bác sĩ thất bại!")
        }
    };
    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);
        const isValidForm = validateForm();
        if (!isValidForm) {
            return;
        }
        try {
            handleEditDoctor(doctors);
        } catch (error) {
        } finally {
            setIsLoading(false);
        }
    };
    return (
        <div className="dark:bg-[#1f2430] bg-white p-4 shadow-md overflow-auto min-w-[calc(100vw)]">
            {isLoading ? (
                <Loading />
            ) : (
                <main class="min-h-[calc(100vh-150px)] md:mt-8 bg-white dark:bg-[#1f2430] transition-all main">
                    <div className="p-6">
                        <div class="grid grid-cols-1 grid-rows-1 ml-36">
                            <div className="relative max-w-xs mt-7">
                                <select
                                    name="dateOfWeek"
                                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-white dark:bg-[#1f2430] dark:text-slate-50"
                                    id=""
                                    value={state.inputDateOfWeek}
                                    // onInput={handleDateOfWeekSelect}
                                    onChange={handleDateOfWeedChange}
                                >
                                    <option value="-1">chọn thứ khám</option>
                                    <option value="1">thứ hai</option>
                                    <option value="2">thứ ba</option>
                                    <option value="3">thứ tư</option>
                                    <option value="4">thứ năm</option>
                                    <option value="5">thứ sáu</option>
                                    <option value="6">thứ bảy</option>
                                    <option value="0">chủ nhật</option>
                                </select>
                                <label
                                    htmlFor=""
                                    className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-white dark:bg-[#1f2430] bg-white transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500"
                                >
                                    Chọn thứ
                                </label>
                            </div>
                            <div className="mt-10">
                                <label className="block text-gray-600 font-medium mb-2 dark:text-white">Chọn giờ khám:</label>
                                <div className="grid grid-cols-2 sm:grid-cols-4 gap-4  dark:text-zinc-500">
                                    {[
                                        "07:00-08:00",
                                        "08:00-09:00",
                                        "09:00-10:00",
                                        "10:00-11:00",
                                        "13:00-14:00",
                                        "14:00-15:00",
                                        "15:00-16:00",
                                        "16:00-17:00",
                                    ].map((timeSlot, index) => (
                                        <label
                                            key={index}
                                            className="flex items-center space-x-3"
                                        >
                                            <input
                                                id={index}
                                                value={timeSlot}
                                                type="checkbox"
                                                onClick={(e) => handleSelectTime(e, index)}
                                                className="form-checkbox h-5 w-5 rounded text-blue-600"
                                                checked={checkboxStates[index]}
                                            />
                                            <span className="text-gray-700 dark:text-white">{timeSlot}</span>
                                        </label>
                                    ))}
                                </div>
                            </div>
                            <div className="flex justify-between mt-12">
                                <button
                                    onClick={handleSubmit}
                                    className="dark:bg-white dark:text-black bg-blue-500 font-medium text-white py-2 px-6 rounded-md hover:bg-blue-600 focus:outline-none"
                                >
                                    Cập Nhật
                                </button>
                            </div>
                        </div>
                    </div>
                </main>
            )}
        </div>
    );
};

export default UpdateTimeWorkDocTor;