import React, { useEffect, useState } from 'react';
import LoadingEdit from '../../components/Loading/LoadingEdit';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { createAxios } from '../../createInstance';
import { loginSuccess } from '../../redux/authSlice';
import doctorApi from '../../api/doctorApi';

const SchedeluPage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [schedules, setSchedules] = useState([]);
    const user = useSelector((state) => state.auth.login?.currentUser);
    const DISPATCH = useDispatch();
    const navigated = useNavigate();
    let accessToken = user?.accessToken;
    let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
    useEffect(() => {
        try {
            if (!user) {
                navigated("/login");
            } else {
                if (user.role !== "doctor") {
                    navigated("/");
                } else {
                    if (!accessToken || typeof accessToken !== "string") {
                        throw new Error("Token không hợp lệ");
                    }
                    const fetchInfo = async () => {
                        try {
                            const data = await doctorApi.getInfomationByCustomerForDoctorByDoctorEmail(
                                axiosJWT,
                                navigated,
                                accessToken,
                                user.email
                            );
                            setSchedules(data.data);
                        } catch (error) {
                            console.error("Lỗi khi lấy dữ liệu từ API:", error);
                        }

                    };
                    fetchInfo();
                }
            }
        } catch (error) { }
    }, []);
    return (
        <div className='bg-white dark:bg-[#1f2430] min-h-[calc(100vh-80px)] min-w-[calc(100vw)]'>
            <table className={`w-full min-w-[800px] bg-white ${isLoading ? 'flex flex-col justify-center items-center' : ''}`} data-tab-for="order" data-page="active">
                <thead className="w-full">
                    <tr>
                        <th style={{ minWidth: '510px', tableLayout: 'fixed' }} className="border-b-cyan-50 whitespace-nowrap text-[15px] uppercase tracking-wide font-semibold text-gray-600 py-2 px-4 bg-gray-200 dark:bg-gray-800 dark:text-gray-300 text-left">Tên</th>
                        <th style={{ minWidth: '510px', tableLayout: 'fixed' }} className="border-b-cyan-50 whitespace-nowrap text-[15px] uppercase tracking-wide font-semibold text-gray-600 py-2 px-4 bg-gray-200 dark:bg-gray-800 dark:text-gray-300 text-center">Ngày tháng</th>
                        <th style={{ minWidth: '510px', tableLayout: 'fixed' }} className="border-b-cyan-50 whitespace-nowrap text-[15px] uppercase tracking-wide font-semibold text-gray-600 py-2 px-4 bg-gray-200 dark:bg-gray-800 dark:text-gray-300 text-center">Giờ</th>
                    </tr>
                </thead>

                {
                    isLoading ? (
                        <LoadingEdit />
                    ) : (
                        <tbody>
                            {schedules?.map((items) => (
                                <tr key={items?.index} className='even:bg-blue-50 odd:bg-orange-50 dark:even:bg-gray-300 dark:odd:bg-gray-200'>
                                    <td className="py-2 px-5 border-b border-b-cyan-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                        <span className="text-[16px] font-medium text-gray-600">{items?.name}</span>
                                    </td>
                                    <td className="py-2 px-5 border-b border-b-cyan-50 whitespace-nowrap overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                        <span className="text-[16px] font-medium text-gray-600">{`${items?.day}/${items?.month}/${items?.year}`}</span>
                                    </td>
                                    <td className="py-2 px-5 border-b border-b-cyan-50 whitespace-nowrap overflow-auto text-center" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                        <span className="text-[16px] font-medium text-gray-600">{`${items?.timeStart}-${items?.timeEnd}`}</span>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    )
                }
            </table>
        </div>
    );
};

export default SchedeluPage;