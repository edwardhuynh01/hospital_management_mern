import React from "react";
import '../../components/DoctorNavbar/doctorNavbar.css';
import logo from '../../assets/img/sinhto.png';
import search_icon_light from '../../assets/icons/search-w.png';
import search_icon_dark from '../../assets/icons/search-b.png';
import toggle_light from '../../assets/icons/night.png';
import toggle_dark from '../../assets/icons/day.png';

const doctorNavbar = ({ theme, setTheme }) => {
    const toggleTheme = () => {
        theme === "light" ? setTheme("dark") : setTheme("light");
    }

    return (
        <div className="doctor-navbar">
            <img src={logo} alt="" className="doctor-logo" />
            <ul className="doctor-ul">
                <li><a href="/doctor/schedule" className={theme === 'light' ? "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-300 to-white hover:shadow-xl  active:bg-gray-300 active:shadow-inner active:scale-95" :
                    "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-100 to-black hover:shadow-xl hover:shadow-gray-500 active:bg-gray-300 active:shadow-inner active:scale-95"}>Xem lịch khám</a></li>
                <li><a href="/doctor/updateTimeWorkDocTor" className={theme === 'light' ? "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-300 to-white hover:shadow-xl  active:bg-gray-300 active:shadow-inner active:scale-95" :
                    "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-100 to-black hover:shadow-xl hover:shadow-gray-500 active:bg-gray-300 active:shadow-inner active:scale-95"}>Giờ khám</a></li>
                {/* <li><a href="/doctor/profile" className={theme === 'light' ? "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-300 to-white hover:shadow-xl  active:bg-gray-300 active:shadow-inner active:scale-95" :
                    "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-100 to-black hover:shadow-xl hover:shadow-gray-500 active:bg-gray-300 active:shadow-inner active:scale-95"}>Profile</a></li> */}
                <li><a href="/" className={theme === 'light' ? "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-300 to-white hover:shadow-xl  active:bg-gray-300 active:shadow-inner active:scale-95" :
                    "inline-block px-4 py-3 rounded-lg transform transition duration-100 ease-in-out hover:bg-gradient-to-t from-gray-100 to-black hover:shadow-xl hover:shadow-gray-500 active:bg-gray-300 active:shadow-inner active:scale-95"}>Thoát</a></li>
            </ul>
            {/* <div className="doctor-search-box">
                <input type="text" placeholder="Tìm kiếm..." />
                <img src={theme === 'light' ? search_icon_light : search_icon_dark} alt="" />
            </div> */}
            <img src={theme === 'light' ? toggle_light : toggle_dark} onClick={() => { toggleTheme() }} alt="" className="doctor-toggle-icon" />
        </div>
    );
}

export default doctorNavbar;