import React, { useEffect, useReducer, useState } from "react";
import Modal from 'react-modal';
import { toast } from "react-toastify";
import Loading from "../Loading/Loading";
import accountApi from "../../api/accountApi";

const initialState = {
    // inputName: "",
    inputUsername: "",
    inputStatus: "",
    inputRole: "",
    inputPhoneNumber: "",
};

function reducer(state, action) {
    switch (action.type) {
        // case "SET_NAME_VALUE":
        //     return { ...state, inputName: action.payload };
        case "SET_USERNAME_VALUE":
            return { ...state, inputUsername: action.payload };
        // case "SET_STATUS_VALUE":
        //     return { ...state, inputStatus: action.payload };
        case "SET_ROLE_VALUE":
            return { ...state, inputRole: action.payload };
        case "SET_PHONENUMBER_VALUE":
            return { ...state, inputPhoneNumber: action.payload };
        case "SET_ACCOUNT":
            return { ...state, ...action.payload };
        default:
            return state;
    }
}

const EditAccountForm = ({ isOpen, onRequestClose, onSubmit, isId, axiosJWT, navigate, accessToken }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (event) => {
        const { name, value } = event.target;
        dispatch({ type: `SET_${name.toUpperCase()}_VALUE`, payload: value });
    };

    const validateForm = () => {
        let isValid = true;
        // if (state.inputName === "") {
        //     isValid = false;
        // }
        if (state.inputUsername === "") {
            isValid = false;
        }
        // if (state.inputStatus === "") {
        //     isValid = false;
        // }
        if (state.inputRole === "") {
            isValid = false;
        }
        if (state.inputPhoneNumber === "") {
            isValid = false;
        }
        return isValid;
    };

    useEffect(() => {
        const fetchAccount = async () => {
            try {
                const response = await accountApi.getAccountById(axiosJWT, navigate, accessToken, isId);
                dispatch({ type: "SET_ACCOUNT", payload: response.data });
            } catch (error) {
                console.error('Error fetching account:', error);
            }
        };
        fetchAccount();
    }, [isId, axiosJWT, navigate, accessToken]);

    const handleSubmit = async (e) => {
        try {
            e.preventDefault();
            setIsLoading(true);
            const updatedData = {
                // name: state.inputName,
                // username: state.inputUsername,
                // status: state.inputStatus,
                role: state.inputRole,
                // phoneNumber: state.inputPhoneNumber,
            };
            await onSubmit(updatedData);
        } catch (error) {
            toast.error("Đã xảy ra lỗi khi cập nhật tài khoản.");
            console.error("Error updating account:", error);
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Edit Account"
            className="bg-white p-4 rounded-lg shadow-md overflow-auto max-h-screen"
        >
            {isLoading ? (
                <Loading />
            ) : (
                <main className="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 md:mt-8 bg-white transition-all main">
                    <div className="p-6">
                        <div className="grid grid-cols-1 grid-rows-1">
                            <h2 className="text-2xl font-bold mb-4 mt-2">Sửa Thông Tin Tài Khoản</h2>
                            <form onSubmit={handleSubmit} className="mt-32 space-y-4 flex flex-col items-center">
                                {/* <div className="relative w-full max-w-md dark:bg-zinc-900">
                                    <input type="text" value={state.inputUsername} id="username" name="username" placeholder=" " onChange={handleChange} required className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="username" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Người sở hữu</label>
                                </div> */}
                                {/* <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="text" value={state.inputName} id="name" name="name" placeholder=" " onChange={handleChange} required className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="name" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Tên đăng nhập</label>
                                </div> */}
                                {/* <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="text" value={state.inputStatus} id="status" name="status" placeholder=" " onChange={handleChange} required className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="status" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Trạng thái</label>
                                </div> */}
                                <div className="relative w-full max-w-md dark:bg-zinc-900">
                                    {/* <input type="text" value={state.inputRole} id="role" name="role" placeholder=" " onChange={handleChange} required className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input> */}
                                    <select id="role" name='role' onChange={handleChange} selected className='w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500'>
                                        <option value="Chọn vị trí" selected disabled hidden></option>
                                        <option value="admin">admin</option>
                                        <option value="doctor">doctor</option>
                                        <option value="customer">customer</option>
                                    </select>
                                    <label htmlFor="role" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Vị trí</label>
                                </div>
                                {/* <div className="relative w-full max-w-md dark:bg-zinc-900">
                                    <input type="text" value={state.inputPhoneNumber} id="phoneNumber" name="phoneNumber" placeholder=" " onChange={handleChange} required className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="phoneNumber" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Số điện thoại</label>
                                </div> */}
                                <div className="flex justify-between py-7">
                                    <button type="submit" className="mr-20 bg-blue-500 text-white py-2 px-6 rounded-md hover:bg-blue-600 focus:outline-none">Cập nhật thông tin</button>
                                    <button type="button"
                                        onClick={() => {
                                            onRequestClose();
                                        }}
                                        className="ml-20 bg-gray-300 text-gray-700 py-2 px-6 rounded-md hover:bg-gray-400 focus:outline-none">Đóng</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
            )}
        </Modal>
    );
};

export default EditAccountForm;