import React from "react";

const ModalAccountDetails = ({ showModal, setShowModal, selectedAccount }) => {
  const formatDate = (date) => {
    if (!date) return "Invalid Date"; // Trả về chuỗi thông báo nếu không có giá trị ngày
    const dateObj = new Date(date);
    if (isNaN(dateObj.getTime())) return "Invalid Date"; // Kiểm tra xem dateObj có hợp lệ không
    return dateObj.toISOString().split("T")[0];
  };
  return (
    <>
      {showModal && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div
            className="fixed inset-0 bg-black opacity-50"
            onClick={() => setShowModal(false)}
          ></div>
          <div className="bg-white p-6 rounded-lg shadow-lg z-10">
            <button
              className="absolute top-2 right-2 text-gray-600 hover:text-gray-900"
              onClick={() => setShowModal(false)}
            >
              &times;
            </button>
            {selectedAccount && (
              <div className="flex gap-3 flex-col">
                <h1
                  className={`text-2xl font-semibold ${
                    selectedAccount.isPayment === "Đã thanh toán"
                      ? "text-green-600"
                      : "text-red-700"
                  }`}
                >
                  {selectedAccount.isPayment}
                </h1>
                <h2 className="text-xl font-bold mb-4">Chi tiết tài khoản</h2>
                <p>
                  <span className="font-semibold">Tên tài khoản: </span>
                  {selectedAccount.username}
                </p>
                <p>
                  <span className="font-semibold">Tên bệnh nhân: </span>
                  {selectedAccount.fullName}
                </p>
                <p>
                  <span className="font-semibold">Mã định danh: </span>
                  {selectedAccount.IdentityCardNumber}
                </p>
                <p>
                  <span className="font-semibold">Email: </span>
                  {selectedAccount.email}
                </p>
                <p>
                  <span className="font-semibold">Số điện thoại: </span>
                  {selectedAccount.phoneNumber}
                </p>
                <p>
                  <span className="font-semibold">Giới tính: </span>
                  {selectedAccount.sex}
                </p>
                <p>
                  <span className="font-semibold">Ngày sinh: </span>
                  {formatDate(selectedAccount.date)}
                </p>
                <p>
                  <span className="font-semibold">Địa chỉ: </span>
                  {selectedAccount.address}
                </p>
                <p>
                  <span className="font-semibold">Ngày đặt khám: </span>
                  {selectedAccount.appointmentDay}
                </p>
                <p>
                  <span className="font-semibold">Thời gian khám: </span>
                  {selectedAccount.appointmentTime}
                </p>
                <p>
                  <span className="font-semibold">Giá khám: </span>
                  {selectedAccount.price}
                </p>
                <p>
                  <span className="font-semibold">Chỉ định bác sĩ: </span>
                  {selectedAccount.doctorName}
                </p>
              </div>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default ModalAccountDetails;
