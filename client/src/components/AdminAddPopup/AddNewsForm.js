import React, { useState, useRef } from "react";
import Modal from "react-modal";
import { Editor } from "@tinymce/tinymce-react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { toast } from "react-toastify";
import axios from "axios";
import Loading from "../Loading/Loading";
const AddNewsForm = ({ isOpen, onRequestClose, onSubmit, isAdded }) => {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, dispatch, loginSuccess);
  const [isLoading, setIsLoading] = useState(false);
  const useDarkMode = window.matchMedia("(prefers-color-scheme: dark)").matches;
  const editorRef = useRef(null);

  const [fileName, setFileName] = useState("Không có tệp nào được chọn");
  const [formData, setFormData] = useState({
    title: "",
    teaser: "",
    content: "",
    author: user._id,
    mainImage: null,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleEditorChange = (content) => {
    setFormData((prevData) => ({
      ...prevData,
      content,
    }));
  };

  // const handleFileChange = (e) => {
  //   const file = e.target.files[0];
  //   setFormData((prevData) => ({
  //     ...prevData,
  //     image: file,
  //   }));
  //   setFileName(file ? file.name : "Không có tệp nào được chọn");
  // };

  const validateForm = () => {
    let isValid = true;
    if (formData.title.trim() === "") {
      isValid = false;
    }
    if (formData.teaser.trim() === "") {
      isValid = false;
    }
    if (formData.content.trim() === "") {
      isValid = false;
    }
    return isValid;
  };

  // const handleChooseFile = () => {
  //   return new Promise((resolve, reject) => {
  //     const btnChooseFile = document.getElementById("image-upload");

  //     const file = btnChooseFile.files[0];
  //     if (!file) {
  //       return toast.error("Vui lòng chọn hình ảnh");
  //     }

  //     const reader = new FileReader();
  //     reader.readAsDataURL(file);
  //     reader.addEventListener("load", () => {
  //       const data = reader.result.split(",")[1];
  //       const postData = {
  //         name: formData.title,
  //         type: file.type,
  //         data: data,
  //       };

  //       postFile(postData)
  //         .then((response) => resolve(response))
  //         .catch((error) => reject(error));
  //     });
  //     reader.onerror = () => {
  //       reject(toast.error("Failed to read file"));
  //     };
  //   });
  // };

  // const postFile = async (postData) => {
  //   try {
  //     const response = await fetch(
  //       "https://script.google.com/macros/s/AKfycbxA2nd3dNnMh-UkBFJ4nGTE6mqosePHJp30gX2PILSea90yqgKaOT4joqP7zFlyTyU/exec",
  //       {
  //         method: "POST",
  //         body: JSON.stringify(postData),
  //       }
  //     );
  //     const data = await response.json();
  //     formData.mainImage = data.link;
  //     return data;
  //     //   console.log(data);
  //     //   console.log(formData);
  //   } catch (error) {
  //     console.log("có lỗi xảy ra vui lòng thử lại", error);
  //   }
  // };
  const [base64String, setBase64String] = useState();

  const handleFileChangeBase64 = (event) => {
    const file = event.target.files[0];
    if (file) {
      setFileName(file ? file.name : "Không có tệp nào được chọn");
      const reader = new FileReader();
      reader.onloadend = () => {
        const base64 = reader.result.replace("data:", "").replace(/^.+,/, "");
        setBase64String(base64);
        formData.mainImage = base64;
      };
      reader.readAsDataURL(file);
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const isValidForm = validateForm();
      if (isValidForm) {
        // const image = await handleChooseFile();
        formData.mainImage = base64String;
        onSubmit(formData);
        setFileName("Không có tệp nào được chọn");
        setBase64String(null);
        formData.mainImage = "";
      } else {
        toast.error("Vui lòng điền đầy đủ thông tin trước khi thêm bản tin!");
      }
    } catch (error) {
      console.log(error)
    }
    finally {
      setIsLoading(false);
    }
  };
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel="Add News Form"
      className="bg-white p-4 rounded-lg shadow-md overflow-auto max-h-screen"
    >
      {isLoading ? (
        <Loading />
      ) : (
        <main className="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 md:mt-8 bg-white transition-all main">
          <div className="p-6">
            <div className="grid grid-cols-1 grid-rows-1">
              <h2 className="text-2xl font-bold mb-4">Thêm Bản Tin Mới</h2>
              <form onSubmit={handleSubmit} className="space-y-4">
                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                  <input type="text" id="title" name="title" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                  <label htmlFor="title" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Tiêu Đề</label>
                </div>
                {/* <input
                  type="text"
                  name="title"
                  placeholder="Tiêu Đề"
                  onChange={handleChange}
                  required
                  className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                /> */}
                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                  <textarea type="text" id="teaser" name="teaser" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></textarea>
                  <label htmlFor="teaser" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Giới thiệu</label>
                </div>
                {/* <textarea
                  type="text"
                  name="teaser"
                  placeholder="Giới thiệu"
                  onChange={handleChange}
                  required
                  className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                /> */}

                <Editor
                  className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                  apiKey="9j35r9v341i5u1rhmipsbdens0tm43l5p85ew6tle1btezw6"
                  name="description"
                  onInit={(_evt, editor) => (editorRef.current = editor)}
                  onEditorChange={handleEditorChange}
                  init={{
                    height: 230,
                    max_height: 340,
                    selector: "textarea#open-source-plugins",
                    plugins:
                      "preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons accordion",
                    editimage_cors_hosts: ["picsum.photos"],
                    menubar: "file edit insert format table help",
                    toolbar:
                      "undo redo | accordion accordionremove | blocks fontfamily fontsize | bold italic underline strikethrough | align numlist bullist | link image | table media | lineheight outdent indent| forecolor backcolor removeformat | charmap emoticons | code fullscreen preview | save print | pagebreak anchor codesample | ltr rtl",
                    autosave_ask_before_unload: true,
                    autosave_interval: "30s",
                    autosave_prefix: "{path}{query}-{id}-",
                    autosave_restore_when_empty: false,
                    autosave_retention: "2m",
                    image_advtab: true,
                    // image_class_list: [
                    //   { title: "None", value: "" },
                    //   { title: "Some class", value: "class-name" },
                    // ],
                    // link_list: [
                    //   { title: "My page 1", value: "https://www.tiny.cloud" },
                    //   { title: "My page 2", value: "http://www.moxiecode.com" },
                    // ],
                    // image_list: [
                    //   { title: "My page 1", value: "https://www.tiny.cloud" },
                    //   { title: "My page 2", value: "http://www.moxiecode.com" },
                    // ],

                    importcss_append: true,
                    file_picker_callback: function (callback, value, meta) {
                      // Chỉ mở file khi nhấp vào nút chọn hình ảnh
                      if (meta.filetype === 'image') {
                        // Tạo một đối tượng input để chọn file từ máy tính
                        var input = document.createElement('input');
                        input.setAttribute('type', 'file');
                        input.setAttribute('accept', 'image/*');

                        // Xử lý khi có sự thay đổi trong file được chọn
                        input.onchange = function () {
                          var file = this.files[0]; // Lấy file đầu tiên từ danh sách file được chọn

                          var reader = new FileReader();
                          reader.onload = function () {
                            var base64 = reader.result; // Chuyển đổi file thành dạng base64
                            callback(base64, { title: file.name }); // Gọi callback với dữ liệu base64 và tiêu đề là tên file
                          };
                          reader.readAsDataURL(file); // Đọc file dưới dạng base64
                        };

                        input.click(); // Kích hoạt sự kiện click để chọn file từ máy tính
                      }
                    },
                    image_caption: true,
                    quickbars_selection_toolbar:
                      "bold italic | quicklink h2 h3 blockquote quickimage quicktable",
                    noneditable_class: "mceNonEditable",
                    toolbar_mode: "sliding",
                    contextmenu: "link image table",
                    skin: useDarkMode ? "oxide-dark" : "oxide",
                    content_css: useDarkMode ? "dark" : "default",
                    content_style:
                      "body { font-family:Helvetica,Arial,sans-serif; font-size:16px }",
                  }}
                />
                <div>
                  <label
                    htmlFor="image-upload"
                    className="block text-sm font-medium text-gray-700 mb-2"
                  >
                    Chọn hình ảnh:
                  </label>
                  <div className="flex items-center">
                    <input
                      type="file"
                      id="image-upload"
                      name="image"
                      accept="image/*"
                      onChange={handleFileChangeBase64}
                      className="hidden"
                    />
                    <label htmlFor="image-upload" className="custom-file-upload cursor-pointer flex items-center justify-center bg-pink-400 text-white py-2 px-4 rounded-md hover:bg-pink-500 focus:outline-none">
                      Chọn tệp
                    </label>
                    <span id="file-name" className="ml-3 text-gray-500">
                      {fileName}
                    </span>
                  </div>
                  <img src={`${formData.mainImage !== null ? `data:image/png;base64,${formData?.mainImage}` : '/images/NoImage.jpg'}`} alt="No Image" className="h-28 w-28 mt-4 rounded-md" />
                </div>
                <div className="flex justify-between py-3">
                  <button
                    type="submit"
                    className="bg-blue-500 text-white py-2 px-6 rounded-md hover:bg-blue-600 focus:outline-none"
                  >
                    Thêm Bản Tin
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      onRequestClose();
                      setFileName("Không có tệp nào được chọn");
                      setBase64String("");
                      formData.mainImage = null;
                    }}
                    className="bg-gray-300 text-gray-700 py-2 px-6 rounded-md hover:bg-gray-400 focus:outline-none"
                  >
                    Đóng
                  </button>
                </div>
              </form>
            </div>
          </div>
        </main>
      )}
    </Modal>
  );
};

export default AddNewsForm;
