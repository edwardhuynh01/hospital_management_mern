import React, { useState } from "react";
import ModalAccountDetails from "./ModalAccountDetails"; // Import component

export default function ViewAppointment() {
  const [showModal, setShowModal] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState(null);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <>
      <button className="" onClick={toggleModal}>
        View Appointment
      </button>
      <ModalAccountDetails
        showModal={showModal}
        setShowModal={setShowModal}
        selectedAccount={selectedAccount}
      />
    </>
  );
}
