import React from "react";
import Navbar from "../AdminNavbar/adminNavbar";
import SideBar from "../AdminSidebar/Sidebar";
import { Outlet } from "react-router-dom";

const AdminLayout = (props) => {
    return (
        <div>
            <SideBar />
            <Navbar />
            <main>
                <Outlet />
            </main>
        </div>
    );
}

export default AdminLayout;