import React from "react";
import styled from "styled-components";
import MedicalExaminationInformationsList from "./MedicalExaminationInformationsList";
const Container = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
function ConfirmMedicalExaminationInformation(props) {
  return (
    <div className="w-full flex flex-wrap">
      <div className="w-full bg-[#f26a8d] text-white px-5 py-2 rounded-t-md font-semibold">
        <h2>Xác nhận thông tin khám</h2>
      </div>
      <Container className="w-full lg:p-4">
        <MedicalExaminationInformationsList></MedicalExaminationInformationsList>
      </Container>
    </div>
  );
}

export default ConfirmMedicalExaminationInformation;
