import React from "react";
import 'boxicons';
import '../../assets/shared/admin.css';
import { Link } from "react-router-dom";
import useStore from '../../utils/useStore';
import { IoLogOutOutline } from "react-icons/io5";

const Navbar = () => {
    const selectedItem = useStore((state) => state.selectedItem);

    return (
        <main className="w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
            <div className="py-2 px-4 bg-white flex items-center shadow-md shadow-black/5 sticky top-0 left-0 z-30">
                <ul className="flex items-center justify-center text-sm ml-2">
                    <li className="mr-2">
                        <Link
                            to="/admin/dashboard"
                            className="text-gray-400 hover:text-gray-600"
                            style={{ fontSize: "15px", fontWeight: "500" }}
                        >
                            Quản Trị
                        </Link>
                    </li>
                    <li
                        className="text-gray-600 mr-2"
                        style={{ fontSize: "15px", fontWeight: "500" }}
                    >
                        /
                    </li>
                    <li
                        className="text-gray-600 mr-2"
                        style={{ fontSize: "15px", fontWeight: "500" }}
                    >
                        {selectedItem}
                    </li>
                </ul>
                <ul className="ml-auto flex items-center">
                    <li className="mr-1 dropdown">
                        <div className="flex items-center">
                            <button
                                type="button"
                                className="flex items-center w-9 h-9 dark:shadow-highlight/4 group ml-4 rounded-md shadow-sm ring-1 ring-gray-900/5 hover:bg-gray-50 focus:outline-none focus-visible:ring-2 focus-visible:ring-sky-500 sm:ml-0 dark:bg-gray-800 dark:ring-0 dark:hover:bg-gray-700 dark:focus-visible:ring-2 dark:focus-visible:ring-gray-400"
                            >
                                <span className="sr-only">
                                    <span className="dark:hidden">Switch to dark theme</span>
                                    <span className="hidden dark:inline">Switch to light theme</span>
                                </span>
                                <svg
                                    width="36"
                                    height="36"
                                    viewBox="-6 -6 36 36"
                                    strokeWidth="2"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    className="fill-sky-100 stroke-sky-400 group-hover:stroke-sky-600 dark:fill-gray-400/20 dark:stroke-gray-400 dark:group-hover:stroke-gray-300"
                                >
                                    <g className="dark:opacity-0">
                                        <path d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"></path>
                                        <path
                                            d="M12 4v.01M17.66 6.345l-.007.007M20.005 12.005h-.01M17.66 17.665l-.007-.007M12 20.01V20M6.34 17.665l.007-.007M3.995 12.005h.01M6.34 6.344l.007.007"
                                            fill="none"
                                        ></path>
                                    </g>
                                    <g className="opacity-0 dark:opacity-100">
                                        <path d="M16 12a4 4 0 1 1-8 0 4 4 0 0 1 8 0Z"></path>
                                        <path
                                            d="M12 3v1M18.66 5.345l-.828.828M21.005 12.005h-1M18.66 18.665l-.828-.828M12 21.01V20M5.34 18.666l.835-.836M2.995 12.005h1.01M5.34 5.344l.835.836"
                                            fill="none"
                                        ></path>
                                    </g>
                                </svg>
                            </button>
                            <div className="hidden sm:block mx-6 lg:mx-4 w-px h-6 bg-gray-200 dark:bg-gray-800"></div>
                            <li className="mr-1">
                                <button
                                    style={{ alignItems: "center" }}
                                    aria-label="Open user account menu"
                                    type="button"
                                    className="bg-transparent p-0"
                                >
                                    <span className="flex align-items-center justify-center">
                                        <div className="mb-2">
                                            <a href="/">
                                                <IoLogOutOutline className="w-10 h-10 mt-2.5 hover:text-[#F4A7B6] rounded-md transition duration-100 ease-in delay-50 hover:scale-125" />
                                            </a>
                                        </div>
                                    </span>
                                </button>
                            </li>
                        </div>
                        <div className="dropdown-menu shadow-md shadow-black/5 z-30 hidden max-w-xs w-full bg-white rounded-md border border-gray-100">
                            <form action="" className="p-4 border-b border-b-gray-100">
                                <div className="relative w-full">
                                    <input
                                        type="text"
                                        className="py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500"
                                        placeholder="Search..."
                                    />
                                    <i className="ri-search-line absolute top-1/2 left-4 -translate-y-1/2 text-gray-400"></i>
                                </div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </main>
    );
};

export default Navbar;
