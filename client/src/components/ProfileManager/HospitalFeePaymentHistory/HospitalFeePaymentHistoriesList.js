import React, { useEffect, useState } from "react";
import styled from "styled-components";
import HospitalFeePaymentHistory from "./HospitalFeePaymentHistory";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import reportApi from "../../../api/reportApi";
const Header6 = styled.h6`
  font-family: Averta, serif;
  font-size: 0.625rem;
  letter-spacing: 0.08rem;
`;
function HospitalFeePaymentHistoriesList(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [reportList, setReportList] = useState([]);

  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await reportApi.getAllReport(
          axiosJWT,
          navigated,
          accessToken
        );
        const databyUserId = data.data.filter((x) => x.account === user._id);
        setReportList(databyUserId.reverse());
      };
      fetchRecord();
    } catch (error) {}
  }, []);
  return (
    <div className="ml-5">
      <div className="flex flex-wrap justify-start items-start">
        <div className="w-full">
          <Header6 className="text-[#627792] font-light">
            PAYMENT HISTORY
          </Header6>
        </div>
        <div className="w-full">
          <h1 className="mb-0 text-2xl font-medium font-sans">
            Lịch sử thanh toán viện phí
          </h1>
        </div>
        <div className="w-full">
          {reportList?.map((reportItem) => (
            <HospitalFeePaymentHistory
              key={reportItem?._id} // Thêm key duy nhất
              reportId={reportItem?._id}
              appId={reportItem?.appointment}
              createdAt={reportItem?.createdAt}
              isPayment={reportItem?.isPayment}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default HospitalFeePaymentHistoriesList;
