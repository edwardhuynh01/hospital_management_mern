import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Notification from "./Notification";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import reportApi from "../../../api/reportApi";
import { navigate } from "ionicons/icons";
import { IoDocumentTextOutline } from "react-icons/io5";
const Header6 = styled.h6`
  font-family: Averta, serif;
  font-size: 0.625rem;
  letter-spacing: 0.08rem;
`;
function NotificationList(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [reportList, setReportList] = useState([]);

  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await reportApi.getAllReport(
          axiosJWT,
          navigated,
          accessToken
        );
        const databyUserId = data.data.filter((x) => x.account === user._id);
        setReportList(databyUserId.reverse());
      };
      fetchRecord();
    } catch (error) {}
  }, []);
  return (
    <div className="ml-5">
      <div className="flex flex-wrap justify-start items-start">
        <div className="w-full">
          <Header6 className="text-[#627792] font-light">NOTIFICATIONS</Header6>
        </div>
        <div className="w-full">
          <h1 className="mb-0 text-2xl font-medium font-sans">
            Danh sách thông báo
          </h1>
          <button
            className="flex gap-2 items-center border border-solid border-[#ff8fab] py-2 px-4 rounded-md text-[#ff8fab] my-4"
            onClick={() => navigated("/profile?component=medicalReports")}
          >
            <IoDocumentTextOutline />
            Phiếu khám bệnh
          </button>
        </div>
        <div className="w-full pt-4 border-t border-solid">
          {reportList?.map((reportItem) => (
            <Notification
              reportId={reportItem?._id}
              createdAt={reportItem?.createdAt}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default NotificationList;
