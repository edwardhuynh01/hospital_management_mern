import React, { useState, useEffect } from "react";
import Navbar from "../DoctorNavbar/doctorNavbar";
import { Outlet } from "react-router-dom";

const DoctorLayout = (props) => {
    const current_theme = localStorage.getItem("current_theme") ? localStorage.getItem("current_theme") : "light";
    const [theme, setTheme] = useState(current_theme);

    useEffect(() => {
        localStorage.setItem("current_theme", theme);
    }, [theme]);

    return (
        <div className={`container ${theme}`}>
            <Navbar theme={theme} setTheme={setTheme} />
            <main>
                <Outlet />
            </main>
        </div>
    );
}

export default DoctorLayout;