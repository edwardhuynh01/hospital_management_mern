import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
  name: "auth",
  initialState: {
    login: {
      currentUser: null,
      isFetching: false,
      error: false,
    },
    register: {
      isFetching: false,
      error: false,
      success: false,
    },
    msg: "",
  },
  reducers: {
    loginStart: (state) => {
      state.login.isFetching = true;
    },
    loginSuccess: (state, action) => {
      state.login.isFetching = false;
      state.login.currentUser = action.payload;
      state.login.error = false;
    },
    loginFalse: (state, action) => {
      state.login.isFetching = false;
      state.login.error = true;
      state.msg = action.payload;
    },
    registerStart: (state) => {
      state.register.isFetching = true;
    },
    registerSuccess: (state) => {
      state.register.isFetching = false;
      state.register.error = false;
      state.register.success = true;
    },
    registerFalse: (state, action) => {
      state.register.isFetching = false;
      state.register.error = true;
      state.register.success = false;
      state.msg = action.payload;
    },
    logoutStart: (state) => {
      state.login.isFetching = true;
    },
    logoutSuccess: (state) => {
      state.login.isFetching = false;
      state.login.currentUser = null;
      state.login.error = false;
    },
    logoutFalse: (state, action) => {
      state.login.isFetching = false;
      state.login.error = true;
      state.msg = action.payload;
    },
  },
});
export const {
  loginStart,
  loginSuccess,
  loginFalse,
  registerStart,
  registerSuccess,
  registerFalse,
  logoutStart,
  logoutSuccess,
  logoutFalse,
} = authSlice.actions;
export default authSlice.reducer;
