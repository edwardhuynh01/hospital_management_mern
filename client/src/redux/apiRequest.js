import axios from "axios";
import mainApi from '../api/mainApi';
import {
  loginFalse,
  loginStart,
  loginSuccess,
  logoutFalse,
  logoutStart,
  logoutSuccess,
  registerFalse,
  registerStart,
  registerSuccess,
} from "./authSlice";
import {
  deleteUserFailed,
  deleteUserStart,
  deleteUserSuccess,
  getUsersFailed,
  getUsersStart,
  getUsersSuccess,
} from "./userSlice";
import { clearAppointment } from "./appointmentSlice";
export const loginUser = async (user, dispatch, navigate) => {
  dispatch(loginStart());
  try {
    const res = await axios.post(`${mainApi}/auth/login`, user, {
      withCredentials: true,
    });
    dispatch(loginSuccess(res.data));
    return res.data;
  } catch (error) {
    console.log(error);
    dispatch(loginFalse(error.response.data));
    return error.response?.data;
  }
};
export const loginGG = async (user, dispatch, navigate) => {
  dispatch(loginStart());
  try {
    const res = await axios.post(
      `${mainApi}/auth/loginGG`,
      user,
      {
        withCredentials: true,
      }
    );
    dispatch(loginSuccess(res.data));
    return res.data;
  } catch (error) {
    dispatch(loginFalse(error.response.data));
    return error.response?.data;
  }
};
// export const registerUser = async (user, dispatch, navigate) => {
//   dispatch(registerStart());
//   try {
//     await axios.post("http://localhost:5000/api/auth/register", user);
//     dispatch(registerSuccess());
//     navigate("/login");
//   } catch (error) {
//     dispatch(registerFalse(error.response.data));
//     return error.response?.data;
//   }
// };
export const registerUser = async (user, dispatch, navigate) => {
  dispatch(registerStart());
  try {
    await axios.post(`${mainApi}/auth/register`, user);
    dispatch(registerSuccess());
    if (typeof navigate === 'function') {
      if (window.location.pathname.includes('/admin/')) {
        navigate("/admin/doctor");
      } else {
        navigate("/login");
      }
    }
  } catch (error) {
    console.log(error);
    dispatch(registerFalse(error.response.data));
    return error.response?.data;
  }
};
export const getAllUsers = async (accessToken, dispatch) => {
  dispatch(getUsersStart());
  try {
    const res = await axios.get(`${mainApi}/user`, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(getUsersSuccess(res.data));
  } catch (error) {
    dispatch(getUsersFailed());
  }
};

export const deleteUser = async (accessToken, dispatch, id) => {
  dispatch(deleteUserStart());
  try {
    const res = await axios.delete(`${mainApi}/user` + id, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(deleteUserSuccess(res.data));
  } catch (error) {
    dispatch(deleteUserFailed(error.response.data));
  }
};

export const logout = async (dispatch, id, navigate, accessToken, axiosJWT) => {
  dispatch(logoutStart());
  try {
    await axiosJWT.post(`${mainApi}/auth/logout`, id, {
      headers: {
        token: `Bearer ${accessToken}`,
      },
      withCredentials: true,
    });
    dispatch(logoutSuccess());
    dispatch(clearAppointment());
    navigate("/login");
  } catch (error) {
    dispatch(logoutFalse(error.response?.data));
  }
};
export const forgotPassword = async (email) => {
  try {
    const res = await axios.post(
      `${mainApi}/auth/forgot`,
      email
    );
    return res.data;
  } catch (error) {
    return error.response?.data;
  }
};
export const resetPasswordShow = async (token, email) => {
  try {
    await axios.get(
      `${mainApi}/auth/reset?token=${token}&email=${email}`
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const resetPassword = async (password, token, email) => {
  try {
    await axios.post(
      `${mainApi}/auth/reset?token=${token}&email=${email}`,
      password
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const confirmEmail = async (token, email) => {
  try {
    await axios.get(
      `${mainApi}/auth/confirmEmail?token=${token}&email=${email}`
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const PaymentReport = async (data) => {
  try {
    const response = await axios.post(
      `${mainApi}/appointment/create-payment-link`,
      data
    );
    return response.data; // Trả về URL từ API
  } catch (error) {
    return error.response?.message;
  }
};
export const successPayment = async (paymentToken, id, dispatch) => {
  try {
    const res = await axios.get(
      `${mainApi}/appointment/success?id=${id}`,
      {
        headers: {
          tokenpayment: `Bearer ${paymentToken}`,
        },
      }
    );
    dispatch(clearAppointment());
    return res.data;
  } catch (error) {
    return error.response;
  }
};
export const makeReportNonPayment = async (dataNonPayment, dispatch) => {
  try {
    const response = await axios.post(
      `${mainApi}/appointment/MakeAppointmentNonPayment`,
      dataNonPayment
    );
    dispatch(clearAppointment());
    return response.data; // Trả về URL từ API
  } catch (error) {
    return error.response?.message;
  }
};
