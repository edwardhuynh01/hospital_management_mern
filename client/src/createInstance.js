import axios from "axios";
import { jwtDecode } from "jwt-decode";
import { clearAppointment } from "./redux/appointmentSlice";
import { logoutSuccess } from "./redux/authSlice";

const refreshToken = async () => {
  try {
    let id;
    const res = await axios.post("http://localhost:5000/api/auth/refresh", id, {
      withCredentials: true,
    });
    console.log(res);
    return res.data;
  } catch (error) {
    console.log(error);
    return error.response?.data;
  }
};
export const createAxios = (user, dispatch, stateSuccess) => {
  const newInstance = axios.create();
  newInstance.interceptors.request.use(
    async (config) => {
      let date = new Date();
      const decodedToken = jwtDecode(user?.accessToken);
      if (decodedToken.exp < date.getTime() / 1000) {
        const data = await refreshToken();
        if (data.message) {
          dispatch(logoutSuccess());
          dispatch(clearAppointment());
        }
        const refreshUser = {
          ...user,
          accessToken: data.accessToken,
        };
        dispatch(stateSuccess(refreshUser));
        config.headers["token"] = "Bearer " + data.accessToken;
      }
      return config;
    },
    (err) => {
      return Promise.reject(err);
    }
  );
  return newInstance;
};
