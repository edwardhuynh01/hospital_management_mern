import dayjs from "dayjs";
export const generateDate = (
  month = dayjs().month(),
  year = dayjs().year()
) => {
  const firstDayOfMonth = dayjs().year(year).month(month).startOf("month");
  const lastDayOfMonth = dayjs().year(year).month(month).endOf("month");
  const arrayOfDay = [];
  //create prefix day
  for (let i = 0; i < firstDayOfMonth.day(); i++) {
    arrayOfDay.push({
      currentMonth: false,
      date: firstDayOfMonth.day(i),
    });
  }
  //gennerate current day
  for (let i = firstDayOfMonth.date(); i <= lastDayOfMonth.date(); i++) {
    arrayOfDay.push({
      date: firstDayOfMonth.date(i),
      currentMonth: true,
      today:
        firstDayOfMonth.date(i).toDate().toDateString() ===
        dayjs().toDate().toDateString(),
    });
  }

  const remaining = 42 - arrayOfDay.length;

  for (
    let i = lastDayOfMonth.date();
    i < lastDayOfMonth.date() + remaining;
    i++
  ) {
    arrayOfDay.push({
      date: lastDayOfMonth.date(i + 1),
      currentMonth: false,
    });
  }

  return arrayOfDay;
};
