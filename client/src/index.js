import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "./config.js";
import { BrowserRouter } from "react-router-dom";
import RouterCustom from "./router";
import { Provider } from "react-redux";
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-confirm-alert/src/react-confirm-alert.css";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <ToastContainer />
        <RouterCustom></RouterCustom>
      </BrowserRouter>
    </PersistGate>
  </Provider>
);
