import axiosclient from "./axiosClient";
import mainApi from './mainApi';
const decode = (id) => {
    return atob(id);
}
const accountApi = {
    getAllAccount: async (axiosJWT, navigate, accessToken) => {
        try {
            const url = `${mainApi}/Account`;
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },

    getAccountById(id) {
        const url = `/account/${decode(id)}`;
        return axiosclient.get(url);
    },

    deleteAccount: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `${mainApi}/Account/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateAccount: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `${mainApi}/Account/${id}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    }
};
export default accountApi;