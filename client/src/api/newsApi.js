import axiosclient from "./axiosClient";
import mainApi from './mainApi';
const decode = (id) => {
  return atob(id);
};
const newsApi = {
  getAllNews(params) {
    const url = "/news";
    return axiosclient.get(url, { params });
  },
  getNewsByTitle(title) {
    const url = `/news/search-list/${title}`;
    return axiosclient.get(url);
  },
  getNewsById(id) {
    const url = `/news/${decode(id)}`;
    return axiosclient.get(url);
  },
  createNews: async (axiosJWT, navigate, accessToken, data) => {
    try {
      const url = `${mainApi}/News`;
      return axiosJWT.post(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  updateNews: async (axiosJWT, navigate, accessToken, id, data) => {
    try {
      const url = `${mainApi}/News/${decode(id)}`;
      return axiosJWT.put(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  // createNews: async (axiosJWT, navigate, accessToken, data) => {
  //   try {
  //     const url = "http://localhost:5000/api/News";
  //     const formData = new FormData();
  //     for (const key in data) {
  //       formData.append(key, data[key]);
  //     }
  //     return axiosJWT.post(url, formData, {
  //       headers: {
  //         token: `Bearer ${accessToken}`,
  //         'Content-Type': 'multipart/form-data',
  //       },
  //     });
  //   } catch (error) {
  //     navigate("/login");
  //   }
  // },
  // updateNews: async (axiosJWT, navigate, accessToken, id, data) => {
  //   try {
  //     const url = `http://localhost:5000/api/News/${decode(id)}`;
  //     const formData = new FormData();
  //     for (const key in data) {
  //       formData.append(key, data[key]);
  //     }
  //     return axiosJWT.put(url, formData, {
  //       headers: {
  //         token: `Bearer ${accessToken}`,
  //         'Content-Type': 'multipart/form-data',
  //       },
  //     });
  //   } catch (error) {
  //     navigate("/login");
  //   }
  // },
  deleteNews: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `${mainApi}/News/${id}`;
      return axiosJWT.delete(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
};
export default newsApi;
