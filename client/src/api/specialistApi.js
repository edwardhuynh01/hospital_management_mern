import axiosclient from "./axiosClient";
import mainApi from './mainApi';
const decode = (id) => {
    return atob(id);
}
const specialistApi = {
    getAllSpecialist(params) {
        const url = '/specialist';
        return axiosclient.get(url, { params });
    },
    getSpecialistById(id) {
        const url = `/specialist/${decode(id)}`;
        return axiosclient.get(url);
    },
    createSpecialist: async (axiosJWT, navigate, accessToken, data) => {
        try {
            const url = `${mainApi}/Specialist`;
            return axiosJWT.post(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateSpecialist: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `${mainApi}/Specialist/${decode(id)}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    deleteSpecialist: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `${mainApi}/Specialist/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default specialistApi;