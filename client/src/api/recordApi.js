import mainApi from './mainApi';
const decode = (id) => {
  return atob(id);
};
const recordApi = {
  getAllRecord: async (axiosJWT, navigate, accessToken) => {
    try {
      const url = `${mainApi}/Record`;
      // console.log(`${mainApi}/Record`)
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  getRecordById: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `${mainApi}/Record/${decode(id)}`;

      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  createRecord: async (axiosJWT, navigate, accessToken, data) => {
    try {
      const url = `${mainApi}/Record`;
      return axiosJWT.post(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  updateRecord: async (axiosJWT, navigate, accessToken, id, data) => {
    try {
      const url = `${mainApi}/Record/${decode(id)}`;
      return axiosJWT.put(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  deleteRecord: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `${mainApi}/Record/${id}`;
      return axiosJWT.delete(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
};
export default recordApi;
