import mainApi from './mainApi';

const decode = (id) => {
  return atob(id);
};
const doctorApi = {
  getAllDoctor: async (axiosJWT, navigate, accessToken) => {
    try {
      const url = `${mainApi}/Doctors`;
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  getDoctorById: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `${mainApi}/Doctors/${id}`;
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  getInfomationByCustomerForDoctorByDoctorEmail: async (axiosJWT, navigate, accessToken, email) => {
    try {
      const url = `${mainApi}/Doctor/schedule/${email}`;
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  getDocterByEmail: async (axiosJWT, navigate, accessToken, email) => {
    try {
      const url = `${mainApi}/Doctor/updateschedule/${email}`;
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  createDoctor: async (axiosJWT, navigate, accessToken, data) => {
    try {
      const url = `${mainApi}/Doctors`;
      return axiosJWT.post(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        },
      });
    } catch (error) {
      navigate("/admin/doctor");
    }
  },
  updateDoctor: async (axiosJWT, navigate, accessToken, id, data) => {
    try {
      const url = `${mainApi}/Doctors/${decode(id)}`;
      return axiosJWT.put(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  updateDoctorByDoctor: async (axiosJWT, navigate, accessToken, id, data) => {
    try {
      const url = `${mainApi}/Doctor/${decode(id)}`;
      return axiosJWT.put(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  deleteDoctor: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `${mainApi}/Doctors/${id}`;
      return axiosJWT.delete(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
};
export default doctorApi;