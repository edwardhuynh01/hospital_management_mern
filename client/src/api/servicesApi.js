import axiosclient from "./axiosClient";
import mainApi from './mainApi';
const decode = (id) => {
    return atob(id);
}
const servicesApi = {
    getAllService(params) {
        const url = '/service';
        return axiosclient.get(url, { params });
    },
    getServiceByName(name) {
        const url = `/service/search-list/${name}`;
        return axiosclient.get(url);
    },
    getServiceById(id) {
        const url = `/service/${decode(id)}`;
        return axiosclient.get(url);
    },
    createService: async (axiosJWT, navigate, accessToken, data) => {
        try {
            const url = `${mainApi}/Service`;
            return axiosJWT.post(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateService: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `${mainApi}/Service/${decode(id)}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    deleteService: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `${mainApi}/Service/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default servicesApi;