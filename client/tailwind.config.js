/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        "main-gradient-to-left": "linear-gradient(to left, #ff8fab, #FB6F92)",
        "main-gradient-to-right": "linear-gradient(to right, #ff8fab, #FB6F92)",
        "main-gradient-to-bottom": "linear-gradient(to bottom, #ff8fab, #FB6F92)",
        "gradient-set-white": "linear-gradient(to bottom, #FFFFFF, #FFFFFF)",
        "second-gradient-to-left": "linear-gradient(to right, #7b2cbf, #9d4edd)",
        "btn-profile": "linear-gradient(to right, transparent 50%, white 50%);",
      },
      boxShadow: {
        "trb": "3px 3px 6px rgba(0, 0, 0, 0.1);"
      },
      screens: {
        "<=xl": { 'max': '1400px' },
        "<=lg": { 'max': '1350px' },
        "<=sm": { 'max': '640px' },
        "<=md": { 'max': '768px' }
      },
    },
  },
  plugins: [],
  darkMode: 'class',
};
