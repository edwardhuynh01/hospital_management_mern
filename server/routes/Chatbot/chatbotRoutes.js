const express = require('express');
const { chatWithBot } = require('../../controllers/Chatbot/chatbotController');
const router = express.Router();

router.post('/chat', chatWithBot);

module.exports = router;