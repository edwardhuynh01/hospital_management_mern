const authController = require("../../controllers/Account/authController");
const middlewareController = require("../../controllers/Account/middlewareController");

const router = require("express").Router();

router.post("/register", authController.registerUser);
router.post("/login", authController.loginUser);
router.post("/loginGG", authController.loginGG);
router.post("/refresh", authController.requestRefreshToken);
router.post("/refreshMobile", authController.requestRefreshTokenForMobile);
router.post("/loginMobile", authController.loginUserForMobile);
router.post("/loginDoctorForMobile", authController.loginDoctorForMobile);
router.post(
  "/logout",
  middlewareController.verifyToken,
  authController.userLogout
);
router.post("/forgot", authController.forgotPassword);
router.get("/reset", authController.resetPasswordShow);
router.post("/reset", authController.resetPassword);
router.get("/confirmEmail", authController.confirmEmail);

module.exports = router;
