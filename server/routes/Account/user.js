const middlewareController = require("../../controllers/Account/middlewareController");
const userController = require("../../controllers/Account/userController");

const router = require("express").Router();
//getalluser
router.get(
  "/",
  middlewareController.verifyTokenAndAdminAuth,
  userController.getAllUser
);
//deleteUser
router.delete(
  "/:id",
  middlewareController.verifyTokenAndAdminAuth,
  userController.deleteUser
);
module.exports = router;
