const paymentController = require("../../controllers/Appointment/PaymentController");

const router = require("express").Router();

router.post("/create-payment-link", paymentController.createPaymentLink);
router.post("/payment-sheet", paymentController.createPaymentLinkStripe);
//ngrok http http://localhost:5000
// https://1b25-2001-ee0-5007-6d50-206d-f3a-449-c731.ngrok-free.app/api/appointment/recive-hook

router.post("/recive-hook", paymentController.reciveHook);
router.post(
  "/MakeAppointmentNonPayment",
  paymentController.MakeAppointmentNonPayment
);
router.post(
  "/MakeAppointmentNonPaymentMobile",
  paymentController.MakeAppointmentNonPaymentMobile
);
router.get(
  "/getInfoPayment",
  paymentController.verifyPaymentToken,
  paymentController.takeInfoPayment
);
router.get(
  "/success",
  paymentController.verifyPaymentToken,
  paymentController.successPayment
);
router.get(
  "/successStripe",
  paymentController.verifyPaymentToken,
  paymentController.successPaymentStripe
);
module.exports = router;
