const NewsRouter = require("./NewsRouter");
const ServiceRouter = require("./ServiceRouter");
const SpecialistRouter = require("./SpecialistRouter");
const RecordRouter = require("./RecordRouter");
const ReportRouter = require("./ReportRouter");
const DoctorRouter = require("./DoctorsRouter");
const AppointmentRouter = require("./AppointmentRouter");
const AccountRouter = require("./AccountRouter");
const router = (app) => {
  app.use("/api", NewsRouter);
  app.use("/api", ServiceRouter);
  app.use("/api", SpecialistRouter);
  app.use("/api", RecordRouter);
  app.use("/api", ReportRouter);
  app.use("/api", DoctorRouter);
  app.use("/api", AppointmentRouter);
  app.use("/api", AccountRouter);
};
module.exports = router;
