const express = require("express");
const router = express.Router();
const middlewareController = require("../../controllers/Account/middlewareController");
const NewsController = require("../../controllers/CustomerManager/NewsController");
const upload = require("../../middleware/upload");

router.get("/News", NewsController.getAllNews);
router.get("/News/search-list/:title", NewsController.getNewsByTitle);
router.get("/News/:id", NewsController.getNewsById);
router.post(
  "/News",
  middlewareController.verifyTokenAndAdminAuth,
  NewsController.createNews
);
router.put(
  "/News/:id",
  middlewareController.verifyTokenAndAdminAuth,
  NewsController.updateNews
);
router.delete(
  "/News/:id",
  middlewareController.verifyTokenAndAdminAuth,
  NewsController.deleteNews
);
// router.post('/News', upload.single('mainImage'), NewsController.createNews);
// router.put('/News/:id', upload.single('mainImage'), NewsController.updateNews);

module.exports = router;
