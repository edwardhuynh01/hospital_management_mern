const express = require("express");
const router = express.Router();
const middlewareController = require("../../controllers/Account/middlewareController");
const SpecialistController = require("../../controllers/CustomerManager/SpecialistController");
router.get("/Specialist", SpecialistController.getAllSpecialists);
router.get("/Specialist/:id", SpecialistController.getSpecialistsById);
router.post(
  "/Specialist",
  middlewareController.verifyTokenAndAdminAuth,
  SpecialistController.createSpecialists
);
router.put(
  "/Specialist/:id",
  middlewareController.verifyTokenAndAdminAuth,
  SpecialistController.updateSpecialists
);
router.delete(
  "/Specialist/:id",
  middlewareController.verifyTokenAndAdminAuth,
  SpecialistController.deleteSpecialists
);
module.exports = router;
