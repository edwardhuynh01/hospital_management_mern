const express = require("express");
const router = express.Router();
const DoctorController = require("../../controllers/CustomerManager/DoctorController");
const middlewareController = require("../../controllers/Account/middlewareController");
router.get(
  "/Doctors",
  middlewareController.verifyToken,
  DoctorController.getAllDocter
);
router.get(
  "/Doctors/:id",
  middlewareController.verifyToken,
  DoctorController.getDocterById
);
router.get(
  "/Doctors/Specialist/:id",
  middlewareController.verifyToken,
  DoctorController.getDocterBySpecId
);
router.get(
  "/Doctor/schedule/:email",
  middlewareController.verifyTokenAndDoctorAuth,
  DoctorController.getInfomationByCustomerForDoctorByDoctorEmail
);
router.get(
  "/Doctor/updateschedule/:email",
  middlewareController.verifyTokenAndDoctorAuth,
  DoctorController.getDocterByEmail
);
router.post(
  "/Doctors",
  middlewareController.verifyTokenAndAdminAuth,
  DoctorController.createDocter
);
router.put(
  "/Doctors/:id",
  middlewareController.verifyTokenAndAdminAuth,
  DoctorController.updateDocter
);
router.put(
  "/Doctor/:id",
  middlewareController.verifyTokenAndDoctorAuth,
  DoctorController.updateDocter
);
router.delete(
  "/Doctors/:id",
  middlewareController.verifyTokenAndAdminAuth,
  DoctorController.deleteDocter
);
module.exports = router;
