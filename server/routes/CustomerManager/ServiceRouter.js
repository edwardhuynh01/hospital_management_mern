const express = require("express");
const router = express.Router();
const middlewareController = require("../../controllers/Account/middlewareController");
const ServiceController = require("../../controllers/CustomerManager/ServiceController");
router.get("/Service", ServiceController.getAllService);
router.get("/Service/search-list/:name", ServiceController.getServiceByName);
router.get("/Service/:id", ServiceController.getServiceById);
router.post(
  "/Service",
  middlewareController.verifyTokenAndAdminAuth,
  ServiceController.createService
);
router.put(
  "/Service/:id",
  middlewareController.verifyTokenAndAdminAuth,
  ServiceController.updateService
);
router.delete(
  "/Service/:id",
  middlewareController.verifyTokenAndAdminAuth,
  ServiceController.deleteService
);
module.exports = router;
