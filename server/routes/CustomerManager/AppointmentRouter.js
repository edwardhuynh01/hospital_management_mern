const express = require("express");
const router = express.Router();
const AppointmentController = require("../../controllers/CustomerManager/AppointmentController");
const middlewareController = require("../../controllers/Account/middlewareController");
router.get(
    "/Appointment",
    middlewareController.verifyToken,
    AppointmentController.getAllAppointment
);
router.get(
    "/Report/Appointment/:id",
    middlewareController.verifyToken,
    AppointmentController.getAppointmentById
);
router.get(
    "/Report/Appointment/Doctor/:email",
    middlewareController.verifyTokenAndDoctorAuth,
    AppointmentController.getAppointmentByDoctorEmail
);
router.post(
    "/Report/Appointment",
    middlewareController.verifyToken,
    AppointmentController.createAppointment
);
router.put(
    "/Report/Appointment/:id",
    middlewareController.verifyToken,
    AppointmentController.updateAppointment
);
router.delete(
    "/Report/Appointment/:id",
    middlewareController.verifyToken,
    AppointmentController.deleteAppointment
);
module.exports = router;
