const express = require("express");
const router = express.Router();
const AccountController = require("../../controllers/Admin/AccountController");
const middlewareController = require("../../controllers/Account/middlewareController");
router.get(
    "/Account/:id",
    middlewareController.verifyTokenAndAdminAuth,
    AccountController.getAccountById
);
router.get(
    "/Account",
    middlewareController.verifyTokenAndAdminAuth,
    AccountController.getAllAccount
);
router.delete(
    "/Account/:id",
    middlewareController.verifyTokenAndAdminAuth,
    AccountController.deleteAccount
);
router.put(
    "/Account/:id",
    middlewareController.verifyTokenAndAdminAuth,
    AccountController.updateAccount
);

module.exports = router;
