const express = require("express");
const router = express.Router();
const RecordController = require("../../controllers/CustomerManager/RecordController");
const middlewareController = require("../../controllers/Account/middlewareController");
router.get(
  "/Record",
  middlewareController.verifyToken,
  RecordController.getAllRecord
);
router.get(
  "/Record/:id",
  middlewareController.verifyToken,
  RecordController.getRecordById
);
router.get(
  "/Record/User/:id",
  middlewareController.verifyToken,
  RecordController.getRecordByUserId
);
router.post(
  "/Record",
  middlewareController.verifyToken,
  RecordController.createRecord
);
router.put(
  "/Record/:id",
  middlewareController.verifyToken,
  RecordController.updateRecord
);
router.delete(
  "/Record/:id",
  middlewareController.verifyToken,
  RecordController.deleteRecord
);
module.exports = router;
