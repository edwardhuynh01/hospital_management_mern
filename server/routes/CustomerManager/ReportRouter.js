const express = require("express");
const router = express.Router();
const ReportController = require("../../controllers/CustomerManager/ReportController");
const middlewareController = require("../../controllers/Account/middlewareController");
router.get(
  "/Report",
  middlewareController.verifyToken,
  ReportController.getAllReport
);
router.get(
  "/Report/:id",
  middlewareController.verifyToken,
  ReportController.getReportById
);
router.post(
  "/Report",
  middlewareController.verifyToken,
  ReportController.createReport
);
router.put(
  "/Report/:id",
  middlewareController.verifyToken,
  ReportController.updateReport
);
router.delete(
  "/Report/:id",
  middlewareController.verifyToken,
  ReportController.deleteReport
);
module.exports = router;
