const jwt = require("jsonwebtoken");

const middlewareController = {
  //verifyToken
  verifyToken: (req, res, next) => {
    const token = req.headers.token;
    if (!token || typeof token !== "string") {
      return res.status(403).json({ message: ["Token không hợp lệ"] });
    }
    if (token) {
      const accessToken = token.split(" ")[1];
      jwt.verify(accessToken, process.env.JWT_ACCESS_KEY, (err, user) => {
        if (err) {
          return res.status(403).json({ message: ["Token hết hạn"] });
        }
        req.user = user;
        next();
      });
    } else {
      return res.status(401).json({ message: ["Bạn chưa đăng nhập"] });
    }
  },
  //verifyAdmin
  verifyTokenAndAdminAuth: (req, res, next) => {
    middlewareController.verifyToken(req, res, () => {
      if (req.user.role === "admin") {
        next();
      } else {
        return res
          .status(403)
          .json({ message: ["Bạn không có quyền admin để làm điều đó"] });
      }
    });
  },
  verifyTokenAndDoctorAuth: (req, res, next) => {
    middlewareController.verifyToken(req, res, () => {
      if (req.user.role === "doctor") {
        next();
      } else {
        return res
          .status(403)
          .json({ message: ["Bạn không có quyền bác sĩ để làm điều đó"] });
      }
    });
  },
};
module.exports = middlewareController;
