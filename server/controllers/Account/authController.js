const User = require("../../models/account");
const { ObjectId } = require("mongodb");
const bcrypt = require("bcrypt");
const registerUserSchema = require("../../schemas/Account/auth");
const jwt = require("jsonwebtoken");
const Mailjet = require("node-mailjet");
const resetPasswordUserSchema = require("../../schemas/Account/resetPassword");
let refreshTokens = [];
const authController = {
  sendConfirmMail: (user, host, resetlink) => {
    const mailjet = Mailjet.apiConnect(
      process.env.MJ_APIKEY_PUBLIC,
      process.env.MJ_APIKEY_PRIVATE
    );

    const request = mailjet.post("send", { version: "v3.1" }).request({
      Messages: [
        {
          From: {
            Email: "nhat.tlm3173@gmail.com",
            Name: "Bệnh viện đa khoa sinh tố dâu",
          },
          To: [
            {
              Email: user?.email,
              Name: user?.username,
            },
          ],
          Subject: "[Bệnh viện đa khoa sinh tố dâu]",
          HTMLPart: `
                <h2>Chào ${user?.username},</h2>
                <br/>
                <p>Bạn vừa mới yêu cầu đăng ký cho tài khoản ${host}. Nhấn vào link bên dưới để tiến hành.</p>
                <br/>
                <a href="${resetlink}">Xác nhận tài khoản</a>
                <br/>
                <p>Nếu bạn không yêu cầu đăng ký, Vui lòng bỏ qua điều này hoặc cho chúng tôi biết. Link xác nhận tài khoản này chỉ tồn tại 30 phút.</p>
                <i>Trường hợp tài khoản được người quản trị cung cấp thì mật khâu là: username của email + <3 chữ số cuối số điện thoại>.</i>
                <br/>
                <p>Cảm ơn,</p> 
                <p>Sinh tố dâu team</p>`,
        },
      ],
    });
    return request;
  },
  registerUser: async (req, res) => {
    try {
      const salt = await bcrypt.genSalt(10);
      const { email, phoneNumber, username, password, confirmPassword } =
        req.body;

      // Validate data
      const { error } = registerUserSchema.validate(req.body, {
        abortEarly: false,
      });

      if (error) {
        const messages = error.details.map((message) => message.message);
        return res.status(400).json({
          message: messages,
        });
      }

      // Check if user exists
      const existUser = await User.findOne({
        $or: [{ email }, { phoneNumber }],
      });

      if (existUser) {
        return res.status(400).json({
          message: ["Email hoặc số điện thoại đã được sử dụng"],
        });
      }

      // Hash password
      const hashed = await bcrypt.hash(password, salt);

      // Create new user
      const newUser = new User({
        email,
        phoneNumber,
        username,
        password: hashed,
      });
      const token = jwt.sign(
        {
          email: email,
        },
        process.env.JWT_ACCESS_KEY,
        { expiresIn: "30m" }
      );
      const host = req.header("host");
      const resetLink = `${req.protocol}://localhost:3000/confirmEmail?token=${token}&email=${email}`;
      authController
        .sendConfirmMail(newUser, host, resetLink)
        .then(async (result) => {
          const user = await newUser.save();
          return res
            .status(200)
            .json({ user, message: ["Gửi mail thành công"] });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ message: ["Gửi mail không thành công!"] });
        });
    } catch (err) {
      return res.status(500).json({ message: [err] });
    }
  },
  generateAccessToken: (user) => {
    return jwt.sign(
      {
        id: user.id,
        role: user.role,
      },
      process.env.JWT_ACCESS_KEY,
      { expiresIn: "30s" }
    );
  },
  generateRefreshToken: (user) => {
    return jwt.sign(
      {
        id: user.id,
        role: user.role,
      },
      process.env.JWT_REFRESH_KEY,
      { expiresIn: "365d" }
    );
  },
  loginUser: async (req, res) => {
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res
          .status(400)
          .json({ message: ["Vui lòng nhập email hoặc password!"] });
      }
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({ message: ["Sai tên đăng nhập!"] });
      }
      const validPassword = await bcrypt.compare(password, user.password);
      if (!validPassword) {
        return res.status(400).json({ message: ["Sai mật khẩu!"] });
      }
      if (user && validPassword) {
        const accessToken = authController.generateAccessToken(user);
        const refreshToken = authController.generateRefreshToken(user);
        refreshTokens.push(refreshToken);
        const oneYearInSeconds = 365 * 24 * 60 * 60;
        const expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + oneYearInSeconds * 1000);
        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          secure: false,
          path: "/",
          sameSite: "strict",
          expires: expireDate,
        });
        const { password, ...other } = user._doc;
        return res.status(200).json({ ...other, accessToken });
      }
    } catch (err) {
      return res.status(500).json({ message: [err] });
    }
  },
  loginUserForMobile: async (req, res) => {
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res
          .status(400)
          .json({ message: ["Vui lòng nhập email hoặc password!"] });
      }
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({ message: ["Sai tên đăng nhập!"] });
      }
      const validPassword = await bcrypt.compare(password, user.password);
      if (!validPassword) {
        return res.status(400).json({ message: ["Sai mật khẩu!"] });
      }
      if (user && validPassword) {
        const accessToken = authController.generateAccessToken(user);
        const refreshToken = authController.generateRefreshToken(user);
        refreshTokens.push(refreshToken);
        const { password, ...other } = user._doc;
        return res.status(200).json({ ...other, accessToken, refreshToken });
      }
    } catch (err) {
      return res.status(500).json({ message: [err] });
    }
  },
  loginDoctorForMobile: async (req, res) => {
    try {
      const { email, password } = req.body;
      if (!email || !password) {
        return res
          .status(400)
          .json({ message: ["Vui lòng nhập email hoặc password!"] });
      }
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({ message: ["Sai tên đăng nhập!"] });
      }
      const validPassword = await bcrypt.compare(password, user.password);
      if (!validPassword) {
        return res.status(400).json({ message: ["Sai mật khẩu!"] });
      }
      if (user && validPassword) {
        if(user.role !=="doctor"){
          return res.status(403).json({ message: ["Vui lòng đăng nhập tài khoản doctor!"] })
        }
        const accessToken = authController.generateAccessToken(user);
        const refreshToken = authController.generateRefreshToken(user);
        refreshTokens.push(refreshToken);
        const { password, ...other } = user._doc;
        return res.status(200).json({ ...other, accessToken, refreshToken });
      }
    } catch (err) {
      return res.status(500).json({ message: [err] });
    }
  },
  loginGG: async (req, res) => {
    try {
      const { email, name, email_verified } = req.body;
      const salt = await bcrypt.genSalt(10);
      // console.log(email, name, email_verified);
      if (!email || !name || !email_verified) {
        return res.status(400).json({ message: ["Sth wrong :<!"] });
      }
      let user = await User.findOne({ email });
      const pass = new ObjectId().toString();
      const hashed = await bcrypt.hash(pass, salt);
      if (!user) {
        const newUser = new User({
          email,
          phoneNumber: new ObjectId(),
          username: name,
          password: hashed,
          emailComfirmed: email_verified,
        });
        user = await newUser.save();
      }

      const accessToken = authController.generateAccessToken(user);
      const refreshToken = authController.generateRefreshToken(user);
      refreshTokens.push(refreshToken);
      const oneYearInSeconds = 365 * 24 * 60 * 60;
      const expireDate = new Date();
      expireDate.setTime(expireDate.getTime() + oneYearInSeconds * 1000);
      res.cookie("refreshToken", refreshToken, {
        httpOnly: true,
        secure: false,
        path: "/",
        sameSite: "strict",
        expires: expireDate,
      });
      const { password, ...other } = user._doc;
      return res.status(200).json({ ...other, accessToken });
    } catch (err) {
      return res.status(500).json({ message: [err] });
    }
  },
  requestRefreshToken: async (req, res) => {
    //Take refresh token from userf
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken)
      return res.status(401).json({ message: ["You're not authenticated"] });
    // if (!refreshTokens.includes(refreshToken)) {
    //   return res.status(403).json("Refresh token is not valid");
    // }
    jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY, async (err, user) => {
      const user1 = await User.findOne({ _id: user.id });
      if (!user1) {
        return res.status(403).json({ message: ["You're not authenticated"] });
      }
      // if (user1.role !== "admin") {
      //   return res.status(401).json({ message: ["You're not authorized"] });
      // }
      if (err) {
        return res.status(401).json({ message: [err] });
      }
      refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
      const newAccessToken = authController.generateAccessToken(user);
      const newRefeshToken = authController.generateRefreshToken(user);
      refreshTokens.push(newRefeshToken);
      const oneYearInSeconds = 365 * 24 * 60 * 60;
      const expireDate = new Date();
      expireDate.setTime(expireDate.getTime() + oneYearInSeconds * 1000);
      res.cookie("refreshToken", newRefeshToken, {
        httpOnly: true,
        secure: false,
        path: "/",
        sameSite: "strict",
        expires: expireDate,
      });
      return res.status(200).json({ accessToken: newAccessToken });
    });
  },
  requestRefreshTokenForMobile: async (req, res) => {
    //Take refresh token from userf
    const { refreshToken } = req.body;
    if (!refreshToken)
      return res
        .status(401)
        .json({ message: ["You're not authenticated asdasd"] });
    // if (!refreshTokens.includes(refreshToken)) {
    //   return res.status(403).json("Refresh token is not valid");
    // }
    jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY, async (err, user) => {
      const user1 = await User.findOne({ _id: user.id });
      if (!user1) {
        return res.status(403).json({ message: ["You're not authenticated"] });
      }
      // if (user1.role !== "admin") {
      //   return res.status(401).json({ message: ["You're not authorized"] });
      // }
      if (err) {
        return res.status(401).json({ message: [err] });
      }
      refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
      const newAccessToken = authController.generateAccessToken(user);
      const newRefeshToken = authController.generateRefreshToken(user);
      refreshTokens.push(newRefeshToken);
      return res
        .status(200)
        .json({ accessToken: newAccessToken, refreshToken: newRefeshToken });
    });
  },
  userLogout: async (req, res) => {
    res.clearCookie("refreshToken");
    refreshTokens = refreshTokens.filter(
      (token) => token !== req.cookies.refreshToken
    );
    return res.status(200).json({ message: ["Đăng xuất thành công!"] });
  },
  sendForgotPasswordMail: (user, host, resetlink) => {
    const mailjet = Mailjet.apiConnect(
      process.env.MJ_APIKEY_PUBLIC,
      process.env.MJ_APIKEY_PRIVATE
    );

    const request = mailjet.post("send", { version: "v3.1" }).request({
      Messages: [
        {
          From: {
            Email: "nhat.tlm3173@gmail.com",
            Name: "Bệnh viện đa khoa sinh tố dâu",
          },
          To: [
            {
              Email: user?.email,
              Name: user?.username,
            },
          ],
          Subject: "[Bệnh viện đa khoa sinh tố dâu] lấy lại mật khẩu",
          HTMLPart: `
                <h2>Chào ${user?.username},</h2>
                <br/>
                <p>Bạn vừa mới yêu cầu lấy lại mật khẩu cho tài khoản ${host}. Nhấn vào link bên dưới để tiến hành.</p>
                <br/>
                <a href="${resetlink}">Lấy lại mật khẩu</a>
                <br/>
                <p>Nếu bạn không yêu cầu lấy lại mật khẩu, Vui lòng bỏ qua điều này hoặc cho chúng tôi biết. Link lấy lại mật khẩu này chỉ tồn tại 30 phút.</p>
                <br/>
                <p>Cảm ơn,</p> 
                <p>Sinh tố dâu team</p>`,
        },
      ],
    });
    return request;
  },
  forgotPassword: async (req, res) => {
    const { email } = req.body;
    if (!email) {
      return res.status(400).json({ message: ["Vui lòng nhập email!"] });
    }
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: ["Email không tồn tại!"] });
    }
    const token = jwt.sign(
      {
        email: email,
      },
      process.env.JWT_ACCESS_KEY,
      { expiresIn: "30m" }
    );
    const host = req.header("host");
    const resetLink = `${req.protocol}://localhost:3000/reset?token=${token}&email=${email}`;
    authController
      .sendForgotPasswordMail(user, host, resetLink)
      .then((result) => {
        return res.status(200).json({ message: ["Gửi mail thành công!"] });
      })
      .catch((error) => {
        return res
          .status(500)
          .json({ message: ["Gửi mail không thành công!"] });
      });
  },
  verifyToken: (token) => {
    try {
      return jwt.verify(token, process.env.JWT_ACCESS_KEY);
    } catch (err) {
      return null;
    }
  },
  resetPasswordShow: async (req, res) => {
    const { token, email } = req.query;
    if (!token) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    }
    const checkToken = authController.verifyToken(token);
    if (!checkToken) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    }
    return res.status(200).json({ message: ["Mời bạn cập nhật mật khẩu"] });
  },
  resetPassword: async (req, res) => {
    const { token, email } = req.query;
    const { password, confirmPassword } = req.body;
    if (!token) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    }
    const checkToken = authController.verifyToken(token);
    if (!checkToken) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    } else {
      const user = await User.findOne({ email: checkToken.email });
      const salt = await bcrypt.genSalt(10);
      if (!user) {
        return res.status(400).json({ message: ["Email không tồn tại!"] });
      }
      const { error } = resetPasswordUserSchema.validate(req.body, {
        abortEarly: false,
      });

      if (error) {
        const messages = error.details.map((message) => message.message);
        return res.status(400).json({
          message: messages,
        });
      }
      const hashed = await bcrypt.hash(password, salt);
      user.password = hashed;
      user.save();
      return res
        .status(200)
        .json({ message: ["Cập nhật mật khẩu thành công"] });
    }
  },
  confirmEmail: async (req, res) => {
    const { token, email } = req.query;
    if (!token) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    }
    const checkToken = authController.verifyToken(token);
    if (!checkToken) {
      return res
        .status(400)
        .json({ message: ["Yêu cầu không hợp lệ hoặc hết hạn"] });
    }
    const user = await User.findOne({ email: checkToken.email });
    if (!user) {
      return res.status(400).json({ message: ["Email không tồn tại!"] });
    }
    user.emailComfirmed = true;
    user.save();
    return res.status(200).json({ message: ["Xác nhận tài khoản thành công"] });
  },
};

module.exports = authController;
