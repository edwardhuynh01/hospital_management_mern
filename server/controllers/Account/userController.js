const User = require("../../models/account");
const isValidObjectId = (id) => {
  // Kiểm tra độ dài của chuỗi
  if (id.length !== 24) {
    return false;
  }
  // Kiểm tra xem chuỗi có chỉ chứa các ký tự hexadecimal không
  return /^[0-9a-fA-F]{24}$/.test(id);
};
const userController = {
  //GET ALL USERS
  getAllUser: async (req, res) => {
    try {
      const user = await User.find();
      return res.status(200).json(user);
    } catch (error) {
      return res.status(500).json(error);
    }
  },
  //DELETE USER
  deleteUser: async (req, res) => {
    try {
      const id = req.params.id;
      if (!id || id.trim() === "") {
        // Kiểm tra xem id có tồn tại và không rỗng không
        return res.status(400).json({
          message: ["Vui lòng cung cấp ID tài khoản muốn xóa"],
        });
      }
      if (!isValidObjectId(id)) {
        return res.status(400).json({
          message: ["ID không hợp lệ vui lòng đưa ra một ID hợp lệ"],
        });
      }
      const user = await User.findById(id);
      if (!user) {
        return res.status(404).json({
          message: ["ID không hợp lệ, không tìm thấy người dùng"],
        });
      }
      return res.status(200).json("Xóa user thành công");
    } catch (error) {
      return res.status(500).json(error);
    }
  },
};
module.exports = userController;
