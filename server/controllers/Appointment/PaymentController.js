const payOS = require("@payos/node");
const Appointment = require("../../models/appointment");
const Report = require("../../models/report");
const Doctor = require("../../models/doctor");
const Record = require("../../models/record");
const Specialist = require("../../models/specialists");
const jwt = require("jsonwebtoken");
const Mailjet = require("node-mailjet");
const payos = new payOS(
  "862f7ecd-6ac9-462d-af3c-551e56594e42",
  "107316ec-842e-4b24-820c-c232432be39c",
  "969ea45ab9df8bba8ff0cc937597569cba679c80395c4579555cffb94b69db00"
);
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY, {
  apiVersion: "2024-10-28.acacia",
  typescript: true,
});
let orderCode = 45; // Giá trị khởi đầu

const generateOrderCode = () => {
  orderCode += 1;
  return orderCode;
};
const paymentController = {
  verifyPaymentToken: (req, res, next) => {
    const token = req.headers.tokenpayment;
    console.log(token);
    if (typeof token !== "string") {
      return res.status(403).json("Token không hợp lệ");
    }
    if (!token) {
      return res.status(401).json("Bạn chưa tạo link thanh toán");
    }
    const accessToken = token.split(" ")[1];
    jwt.verify(accessToken, process.env.JWT_ACCESS_KEY, (err, info) => {
      if (err) {
        return res.status(403).json("Token hết hạn");
      }
      req.info = info;
      next();
    });
  },
  sendReportMail: (record, reportLink) => {
    const mailjet = Mailjet.apiConnect(
      process.env.MJ_APIKEY_PUBLIC,
      process.env.MJ_APIKEY_PRIVATE
    );

    const request = mailjet.post("send", { version: "v3.1" }).request({
      Messages: [
        {
          From: {
            Email: "nhat.tlm3173@gmail.com",
            Name: "Bệnh viện đa khoa sinh tố dâu",
          },
          To: [
            {
              Email: record?.email,
              Name: record?.fullName,
            },
          ],
          Subject: "[Bệnh viện đa khoa sinh tố dâu] Thông tin phiếu khám",
          HTMLPart: `
                <h2>Chào ${record?.fullName},</h2>
                <br/>
                <p>Bạn vừa mới đặt khám. Nhấn vào link bên dưới để xem thông tin phiếu khám.</p>
                <br/>
                <a href="${reportLink}">Thông tin phiếu khám</a>
                <br/>
                <p>Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của chúng tôi. Vui lòng đến đúng giờ hẹn để được khám và điều trị tốt nhất.</p>
                <br/>
                <p>Trân trọng,</p> 
                <p>Sinh tố dâu team</p>`,
        },
      ],
    });
    return request;
  },
  createPaymentLinkStripe: async (req, res) => {
    try {
      const { userId, recordId, listAppointments, amount, isPayment } =
        req.body;
      // const transactions = await await stripe.paymentIntents.retrieve(
      //   "pi_3QNAzO2KGtOY5oGU1vL121eO"
      // );
      // console.log(transactions);
      console.log(listAppointments);

      const user = await Record.findById(recordId);
      const userAddress = user.address.split(", ");
      // console.log(test);
      for (const item of listAppointments) {
        const existAppointment = await Appointment.findOne({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.dayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });
        if (existAppointment) {
          return res.status(400).json({ message: "lịch khám này đã được đặt" });
        }
      }
      const newListAppointmentsPromiseForToken = listAppointments.map(
        async (item) => {
          const newAppointment = new Appointment({
            id: userId,
            recordId: item.recordId,
            dayOfWeek: item.dayOfWeek,
            day: item.day,
            month: item.month,
            year: item.year,
            timeStart: item.timeStart,
            timeEnd: item.timeEnd,
            specialistId: item.specialistId,
            services: "Khám dịch vụ",
            doctorId: item.doctorId,
          });
          return newAppointment;
        }
      );
      const newListAppointmentsForToken = await Promise.all(
        newListAppointmentsPromiseForToken
      );
      console.log(newListAppointmentsForToken);
      const newReport = new Report({
        account: userId,
        record: recordId,
        appointment: newListAppointmentsForToken.map((item) => item._id),
        amount: amount,
        isPayment: isPayment,
      });
      const customer = await stripe.customers.create({
        name: user.fullName,
        email: user.email,
        phone: user.phoneNumber,
        address: {
          country: "VN",
          state: userAddress[3],
          city: userAddress[2],
          line1: userAddress[1],
          line2: userAddress[0],
        },
      });

      const ephemeralKey = await stripe.ephemeralKeys.create(
        { customer: customer.id },
        { apiVersion: "2024-10-28.acacia" }
      );
      const totalPrice = Math.round((amount / 25420) * 100);
      console.log(totalPrice, typeof totalPrice);
      const paymentIntent = await stripe.paymentIntents.create({
        amount: totalPrice,
        currency: "usd",
        customer: customer.id,
        // shipping: defaultShippingDetails,
        // In the latest version of the API, specifying the `automatic_payment_methods` parameter
        // is optional because Stripe enables its functionality by default.
        automatic_payment_methods: {
          enabled: true,
        },
        // payment_method_types: [
        //   "card",
        //   "us_bank_account",
        //   "affirm",
        //   "afterpay_clearpay",
        //   "klarna",
        //   "amazon_pay",
        // ],
      });
      // console.log(customer);
      const PaymentToken = jwt.sign(
        {
          listAppointment: newListAppointmentsForToken,
          report: newReport,
          paymentIntent: paymentIntent.client_secret,
          ephemeralKey: ephemeralKey.secret,
          customer: customer.id,
        },
        process.env.JWT_ACCESS_KEY,
        { expiresIn: "1800s" }
      );
      console.log(
        paymentIntent.client_secret,
        ephemeralKey.secret,
        customer.id,
        process.env.STRIPE_PUBLISHABLE_KEY
      );
      res.status(200).json({
        paymentIntent: paymentIntent.client_secret,
        ephemeralKey: ephemeralKey.secret,
        customer: customer.id,
        publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
        PaymentToken,
      });
    } catch (error) {
      if (error.message.includes("Đơn thanh toán đã tồn tại")) {
        return res.status(400).json({ message: "Đơn thanh toán đã tồn tại" });
      } else {
        // console.log(error);
        return res.status(500).json({ message: error.message });
      }
    }
  },
  createPaymentLink: async (req, res) => {
    try {
      // const ord = req.body;
      const { userId, recordId, listAppointments, amount, isPayment } =
        req.body;
      for (const item of listAppointments) {
        const existAppointment = await Appointment.findOne({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.DayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });
        if (existAppointment) {
          return res.status(400).json({ message: "lịch khám này đã được đặt" });
        }
      }
      const newListAppointmentsPromiseForToken = listAppointments.map(
        async (item) => {
          const newAppointment = new Appointment({
            id: userId,
            recordId: item.recordId,
            dayOfWeek: item.DayOfWeek,
            day: item.day,
            month: item.month,
            year: item.year,
            timeStart: item.timeStart,
            timeEnd: item.timeEnd,
            specialistId: item.specialistId,
            services: "Khám dịch vụ",
            doctorId: item.doctorId,
          });
          return newAppointment;
        }
      );
      const newListAppointmentsForToken = await Promise.all(
        newListAppointmentsPromiseForToken
      );
      const newReport = new Report({
        account: userId,
        record: recordId,
        appointment: newListAppointmentsForToken.map((item) => item._id),
        amount: amount,
        isPayment: isPayment,
      });
      const listItem = [];
      for (const item of listAppointments) {
        const specialist = await Specialist.findById(item.specialistId);
        const appointment = {
          name: specialist.specialistName,
          quantity: 1,
          price: Number(specialist.price),
        };
        listItem.push(appointment);
      }
      const record = await Record.findById(recordId);
      const DOMAIN = "http://localhost:3000";
      const thirtyMinInSeconds = 30 * 60;
      const expireDate = new Date();
      expireDate.setTime(expireDate.getTime() + thirtyMinInSeconds * 1000);
      const expiredAt = Math.floor(expireDate.getTime() / 1000);
      const ord = {
        amount: amount,
        description: "Thanh toán viện phí",
        orderCode: generateOrderCode(),
        buyerName: record.fullName,
        buyerEmail: record.email,
        buyerPhone: record.phoneNumber,
        buyerAddress: record.address,
        items: listItem,
        expiredAt: expiredAt,
        returnUrl: `${DOMAIN}/success`,
        cancelUrl: `${DOMAIN}/cancel`,
      };
      // console.log(ord);
      const paymentLink = await payos.createPaymentLink(ord);
      const PaymentToken = jwt.sign(
        {
          listAppointment: newListAppointmentsForToken,
          report: newReport,
          paymentLinkId: paymentLink.paymentLinkId,
        },
        process.env.JWT_ACCESS_KEY,
        { expiresIn: "1800s" }
      );
      // console.log(paymentLink);
      return res.status(200).json({
        checkoutUrl: paymentLink.checkoutUrl,
        PaymentToken: PaymentToken,
      });
    } catch (error) {
      if (error.message.includes("Đơn thanh toán đã tồn tại")) {
        return res.status(400).json({ message: "Đơn thanh toán đã tồn tại" });
      } else {
        return res.status(500).json({ message: error.message });
      }
    }
  },
  successPayment: async (req, res) => {
    try {
      const { id } = req.query;
      if (!id) {
        return res.status(400).json("Phiếu khám không tồn tại");
      }

      const inforPayment = await payos.getPaymentLinkInformation(id);
      if (inforPayment.status !== "PAID") {
        return res.status(400).json("Phiếu khám chưa thanh toán");
      }
      const existReport = await Report.findOne({ PaymentId: id });
      if (existReport) {
        return res.status(400).json("Mã thanh toán này đã được sử dụng");
      }
      const info = req.info;
      if (!info) {
        return res.status(400).json("Bạn chưa tạo thanh toán");
      }
      if (info.paymentLinkId !== id) {
        return res
          .status(400)
          .json(
            "Vui lòng chờ chuyển hướng đến trang hoàn tất sau khi thanh toán thành công"
          );
      }
      for (const item of info.listAppointment) {
        const existAppointment = await Appointment.findOne({
          id: item.id,
          recordId: item.recordId,
          dayOfWeek: item.dayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });

        if (existAppointment) {
          return res
            .status(400)
            .json(
              "lịch khám này đã được đặt vui lòng liên hệ số điện thoại: 0786571369(Nhật) để được hỗ trợ"
            );
        }
      }
      const newListAppointmentsPromise = info.listAppointment.map(
        async (item) => {
          const newAppointment = new Appointment({
            _id: item._id,
            id: item.id,
            recordId: item.recordId,
            dayOfWeek: item.dayOfWeek,
            day: item.day,
            month: item.month,
            year: item.year,
            timeStart: item.timeStart,
            timeEnd: item.timeEnd,
            specialistId: item.specialistId,
            services: "Khám dịch vụ",
            doctorId: item.doctorId,
          });
          await newAppointment.save();
          return newAppointment;
        }
      );
      const newListAppointments = await Promise.all(newListAppointmentsPromise);
      const newReport = new Report({
        _id: info.report._id,
        account: info.report.account,
        record: info.report.record,
        appointment: info.report.appointment,
        amount: info.report.amount,
        isPayment: info.report.isPayment,
        PaymentId: info.paymentLinkId,
      });
      await newReport.save();
      for (const item of info.listAppointment) {
        const doctor = await Doctor.findById(item.doctorId);

        for (const day of doctor.days) {
          if (day.day === Number(item.day)) {
            for (const time of day.times) {
              if (time.timeStart === item.timeStart) {
                time.isChoose = true;
              }
            }
          }
        }
        await doctor.save();
      }

      // const dataAppointment = inforPayment.
      const record = await Record.findById(info.report.record);
      // console.log(record);
      const RPid = btoa(newReport._id);
      const reportLink = `${req.protocol}://localhost:3000/DetailNotification/${RPid}`;
      paymentController
        .sendReportMail(record, reportLink)
        .then(async (result) => {
          return res.status(200).json({
            data: "Tạo lịch khám thành công",
            newListAppointments,
            newReport,
          });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ message: "Gửi mail không thành công!", error });
        });
    } catch (error) {
      res.status(500).json(error.message);
    }
  },
  successPaymentStripe: async (req, res) => {
    try {
      // const { id } = req.query;
      // if (!id) {
      //   return res.status(400).json("Phiếu khám không tồn tại");
      // }

      // const inforPayment = await payos.getPaymentLinkInformation(id);
      // if (inforPayment.status !== "PAID") {
      //   return res.status(400).json("Phiếu khám chưa thanh toán");
      // }
      // const existReport = await Report.findOne({ PaymentId: id });
      // if (existReport) {
      //   return res.status(400).json("Mã thanh toán này đã được sử dụng");
      // }
      const info = req.info;

      if (!info) {
        return res.status(400).json({ message: "Bạn chưa tạo thanh toán" });
      }
      // if (info.paymentLinkId !== id) {
      //   return res
      //     .status(400)
      //     .json(
      //       "Vui lòng chờ chuyển hướng đến trang hoàn tất sau khi thanh toán thành công"
      //     );
      // }
      for (const item of info.listAppointment) {
        const existAppointment = await Appointment.findOne({
          id: item.id,
          recordId: item.recordId,
          dayOfWeek: item.dayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });
        // console.log(info);
        if (existAppointment) {
          return res
            .status(400)
            .json({
              message:
                "lịch khám này đã được đặt vui lòng liên hệ số điện thoại: 0786571369(Nhật) để được hỗ trợ",
            });
        }
      }
      const newListAppointmentsPromise = info.listAppointment.map(
        async (item) => {
          const newAppointment = new Appointment({
            _id: item._id,
            id: item.id,
            recordId: item.recordId,
            dayOfWeek: item.dayOfWeek,
            day: item.day,
            month: item.month,
            year: item.year,
            timeStart: item.timeStart,
            timeEnd: item.timeEnd,
            specialistId: item.specialistId,
            services: "Khám dịch vụ",
            doctorId: item.doctorId,
          });
          await newAppointment.save();
          return newAppointment;
        }
      );
      const newListAppointments = await Promise.all(newListAppointmentsPromise);

      const newReport = new Report({
        _id: info.report._id,
        account: info.report.account,
        record: info.report.record,
        appointment: info.report.appointment,
        amount: info.report.amount,
        isPayment: info.report.isPayment,
        PaymentId: info.paymentLinkId,
      });
      await newReport.save();
      for (const item of info.listAppointment) {
        const doctor = await Doctor.findById(item.doctorId);
        console.log(doctor);
        for (const day of doctor.days) {
          if (day.day === Number(item.day)) {
            for (const time of day.times) {
              console.log(time, item.timeStart);
              if (time.timeStart === item.timeStart) {
                time.isChoose = true;
              }
            }
          }
        }
        await doctor.save();
      }

      // const dataAppointment = inforPayment.
      const record = await Record.findById(info.report.record);
      // console.log(record);
      const RPid = btoa(newReport._id);
      const reportLink = `${req.protocol}://localhost:3000/DetailNotification/${RPid}`;
      paymentController
        .sendReportMail(record, reportLink)
        .then(async (result) => {
          return res.status(200).json({
            data: "Tạo lịch khám thành công",
            newListAppointments,
            newReport,
          });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ message: "Gửi mail không thành công!", error });
        });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  takeInfoPayment: async (req, res) => {
    try {
      const infor = await payos.getPaymentLinkInformation(
        "89ba8491fec44c568f79a34f7aef3d72"
      );
      // console.log(req.info);
      res.status(200).json(infor);
    } catch (err) {
      res.status(500).json(err.message);
    }
  },
  reciveHook: async (req, res) => {
    console.log(req.body);
    res.status(200).json(req.body);
  },
  MakeAppointmentNonPayment: async (req, res) => {
    try {
      const { userId, recordId, listAppointments, isPayment, amount } =
        req.body;
      for (const item of listAppointments) {
        const existAppointment = await Appointment.findOne({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.DayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });

        if (existAppointment) {
          return res.status(400).json("lịch khám này đã được đặt");
        }
      }

      const newListAppointmentsPromise = listAppointments.map(async (item) => {
        const newAppointment = new Appointment({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.DayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          services: "Khám dịch vụ",
          doctorId: item.doctorId,
        });
        await newAppointment.save();
        return newAppointment;
      });
      const newListAppointments = await Promise.all(newListAppointmentsPromise);
      const newReport = new Report({
        account: userId,
        record: recordId,
        appointment: newListAppointments.map((item) => item._id),
        amount: amount,
        isPayment: isPayment,
      });
      await newReport.save();
      for (const item of listAppointments) {
        const doctor = await Doctor.findById(item.doctorId);
        for (const day of doctor.days) {
          if (day.day === Number(item.day)) {
            for (const time of day.times) {
              if (time.timeStart === item.timeStart) {
                time.isChoose = true;
              }
            }
          }
        }
        await doctor.save();
      }
      const record = await Record.findById(recordId);
      const RPid = btoa(newReport._id);
      const reportLink = `${req.protocol}://localhost:3000/DetailNotification/${RPid}`;
      paymentController
        .sendReportMail(record, reportLink)
        .then(async (result) => {
          return res.status(200).json({
            data: "Tạo lịch khám thành công",
            newListAppointments,
            newReport,
          });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ message: "Gửi mail không thành công!", error });
        });
    } catch (error) {
      return res.status(501).json(error.message);
    }
  },
  MakeAppointmentNonPaymentMobile: async (req, res) => {
    try {
      const { userId, recordId, listAppointments, isPayment, amount } =
        req.body;
      for (const item of listAppointments) {
        const existAppointment = await Appointment.findOne({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.dayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          doctorId: item.doctorId,
        });

        if (existAppointment) {
          return res.status(400).json({ message: "lịch khám này đã được đặt" });
        }
      }

      const newListAppointmentsPromise = listAppointments.map(async (item) => {
        const newAppointment = new Appointment({
          id: userId,
          recordId: item.recordId,
          dayOfWeek: item.dayOfWeek,
          day: item.day,
          month: item.month,
          year: item.year,
          timeStart: item.timeStart,
          timeEnd: item.timeEnd,
          specialistId: item.specialistId,
          services: "Khám dịch vụ",
          doctorId: item.doctorId,
        });
        await newAppointment.save();
        return newAppointment;
      });
      const newListAppointments = await Promise.all(newListAppointmentsPromise);
      const newReport = new Report({
        account: userId,
        record: recordId,
        appointment: newListAppointments.map((item) => item._id),
        amount: amount,
        isPayment: isPayment,
      });
      await newReport.save();
      for (const item of listAppointments) {
        const doctor = await Doctor.findById(item.doctorId);
        for (const day of doctor.days) {
          if (day.day === Number(item.day)) {
            for (const time of day.times) {
              if (time.timeStart === item.timeStart) {
                time.isChoose = true;
              }
            }
          }
        }
        await doctor.save();
      }
      const record = await Record.findById(recordId);
      const RPid = btoa(newReport._id);
      const reportLink = `${req.protocol}://localhost:3000/DetailNotification/${RPid}`;
      paymentController
        .sendReportMail(record, reportLink)
        .then(async (result) => {
          return res.status(200).json({
            data: "Tạo lịch khám thành công",
            newListAppointments,
            newReport,
          });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ message: "Gửi mail không thành công!", error });
        });
    } catch (error) {
      return res.status(501).json(error.message);
    }
  },
};
module.exports = paymentController;
