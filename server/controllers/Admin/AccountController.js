const { json } = require("body-parser");
const account = require("../../models/account");
const getAllAccount = async (req, res) => {
    try {
        const data = await account.find();
        if (data.length < 0) {
            return res.status(404).json({ message: "No account found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};

const deleteAccount = async (req, res) => {
    try {
        const data = await account.deleteOne({ _id: req.params.id });
        if (data.deletedCount === 0) {
            return res.status(404).json({ message: "No account found " });
        }
        res.status(201).json({ message: "Account deleted successfully" });
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
}

const updateAccount = async (req, res) => {
    try {
        const { id } = req.params;
        const updatedData = req.body;
        const updatedAccount = await account.findByIdAndUpdate(id, updatedData, { new: true });
        if (!updatedAccount) {
            return res.status(404).json({ message: "Account not found" });
        }
        res.status(200).json(updatedAccount);
    } catch (error) {
        res.status(500).json({ message: "Đã xảy ra lỗi khi cập nhật tài khoản.", error: error.message });
    }
};

const getAccountById = async (req, res) => {
    try {
        const data = await account.findOne({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No account found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
module.exports = {
    getAllAccount,
    deleteAccount,
    getAccountById,
    updateAccount
};
