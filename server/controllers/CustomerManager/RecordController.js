const { json } = require("body-parser");
const record = require("../../models/record");

const getAllRecord = async (req, res) => {
    try {
        const data = await record.find();
        if (data.length < 0) {
            return res.status(404).json({ message: "No record found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getRecordById = async (req, res) => {
    try {
        const data = await record.findOne({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No record found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getRecordByUserId = async (req, res) => {
    try {
        const data = await record.findOne({ account: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No record found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const createRecord = async (req, res) => {
    try {
        console.log(req.body);
        const data = await record(req.body).save();
        res.status(201).json(data);
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            message: error.message,
        });
    }
};
const updateRecord = async (req, res) => {
    try {
        const data = await record.findByIdAndUpdate(
            { _id: req.params.id },
            req.body,
            { new: true }
        );
        if (data.length < 0) {
            return res.status(404).json({ message: "No record found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const deleteRecord = async (req, res) => {
    try {
        const data = await record.findOneAndDelete({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No record found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};

module.exports = {
    getAllRecord,
    getRecordById,
    getRecordByUserId,
    createRecord,
    updateRecord,
    deleteRecord,
};
