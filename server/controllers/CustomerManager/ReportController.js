const { json } = require("body-parser");
const report = require("../../models/report");

const getAllReport = async (req, res) => {
    try {
        const data = await report.find();
        if (data.length < 0) {
            return res.status(404).json({ message: "No report found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getReportById = async (req, res) => {
    try {
        const data = await report.findOne({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No report found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const createReport = async (req, res) => {
    try {
        console.log(req.body);
        const data = await report(req.body).save();
        res.status(201).json(data);
    } catch (error) {
        return res.status(400).json({
            message: error.message,
        });
    }
};
const updateReport = async (req, res) => {
    try {
        const data = await report.findByIdAndUpdate(
            { _id: req.params.id },
            req.body,
            { new: true }
        );
        if (data.length < 0) {
            return res.status(404).json({ message: "No report found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const deleteReport = async (req, res) => {
    try {
        const data = await report.findOneAndDelete({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No report found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};

module.exports = {
    getAllReport,
    getReportById,
    createReport,
    updateReport,
    deleteReport,
};
