const { json } = require("body-parser");
const News = require("../../models/news");

const getAllNews = async (req, res) => {
  try {
    const data = await News.find();
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const getNewsByTitle = async (req, res) => {

  try {
    const nameToFind = req.params.title;
    const data = await News.find({ title: { $regex: nameToFind, $options: "i" } });

    if (data.length === 0) {
      return res.status(404).json({ message: "No new found" });
    }
    res.status(200).json(data);
  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
};
const getNewsById = async (req, res) => {
  try {
    const data = await News.findOne({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const createNews = async (req, res) => {
  try {
    const data = await News(req.body).save();
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const updateNews = async (req, res) => {
  try {
    const data = await News.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
// const createNews = async (req, res) => {
//   try {
//     const { title, teaser, content, author } = req.body;
//     const image = req.file ? req.file.filename : null;

//     const news = new News({ title, teaser, content, author, image });
//     await news.save();

//     res.status(201).json(news);
//   } catch (error) {
//     res.status(500).json({ message: 'Error creating news', error });
//   }
// };

// const updateNews = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { title, teaser, content, author } = req.body;
//     const image = req.file ? req.file.filename : null;

//     const news = await News.findByIdAndUpdate(id, { title, teaser, content, author, image }, { new: true });
//     res.status(200).json(news);
//   } catch (error) {
//     res.status(500).json({ message: 'Error updating news', error });
//   }
// };
const deleteNews = async (req, res) => {
  try {
    const data = await News.findOneAndDelete({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};

module.exports = {
  getAllNews,
  getNewsById,
  createNews,
  updateNews,
  deleteNews,
  getNewsByTitle,
};
