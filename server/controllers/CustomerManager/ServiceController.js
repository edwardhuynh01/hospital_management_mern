const { json } = require("body-parser");
const services = require("../../models/services");

const getAllService = async (req, res) => {
  try {
    const data = await services.find();
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const getServiceByName = async (req, res) => {
  try {
    const nameToFind = req.params.name;
    const data = await services.find({ name: { $regex: nameToFind, $options: "i" } });

    if (data.length === 0) {
      return res.status(404).json({ message: "No service found" });
    }
    res.status(200).json(data);
  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
};
const getServiceById = async (req, res) => {
  try {
    const data = await services.findOne({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const createService = async (req, res) => {
  try {
    const data = await services(req.body).save();
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const updateService = async (req, res) => {
  try {
    const data = await services.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const deleteService = async (req, res) => {
  try {
    const data = await services.findOneAndDelete({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};

module.exports = {
  getAllService,
  getServiceById,
  createService,
  updateService,
  deleteService,
  getServiceByName,
};
