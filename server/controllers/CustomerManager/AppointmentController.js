const { json } = require("body-parser");
const appointment = require("../../models/appointment");
const Doctor = require("../../models/doctor");

const getAllAppointment = async (req, res) => {
    try {
        const data = await appointment.find();
        if (data.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getAppointmentById = async (req, res) => {
    try {
        const data = await appointment.findOne({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getAppointmentByDoctorEmail = async (req, res) => {
    try {
        const doctor = await Doctor.findOne({ email: req.params.email });

        const data = await appointment.find({ doctorId: doctor._id });
        // console.log(data)
        if (data.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const createAppointment = async (req, res) => {
    try {
        // console.log(req.body);
        const data = await appointment(req.body).save();
        res.status(201).json(data);
    } catch (error) {
        return res.status(400).json({
            message: error.message,
        });
    }
};
const updateAppointment = async (req, res) => {
    try {
        const data = await appointment.findByIdAndUpdate(
            { _id: req.params.id },
            req.body,
            { new: true }
        );
        if (data.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const deleteAppointment = async (req, res) => {
    try {
        const data = await appointment.findOneAndDelete({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};

module.exports = {
    getAllAppointment,
    getAppointmentById,
    createAppointment,
    updateAppointment,
    deleteAppointment,
    getAppointmentByDoctorEmail,
};
