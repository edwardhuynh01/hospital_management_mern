const { json } = require("body-parser");
const docter = require("../../models/doctor");
const Appointment = require("../../models/appointment");
const Record = require("../../models/record");

const getAllDocter = async (req, res) => {
    try {
        const data = await docter.find();
        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getDocterById = async (req, res) => {
    try {
        const data = await docter.findOne({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getDocterBySpecId = async (req, res) => {
    try {
        const data = await docter.find({ specialist: req.params.id });
        console.log(data)
        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const createDocter = async (req, res) => {
    try {
        // console.log(req.body);
        const data = await docter(req.body).save();
        res.status(201).json(data);
    } catch (error) {
        return res.status(400).json({
            message: error.message,
        });
    }
};
const updateDocter = async (req, res) => {
    try {
        const data = await docter.findByIdAndUpdate(
            { _id: req.params.id },
            req.body,
            { new: true }
        );
        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const deleteDocter = async (req, res) => {
    try {
        const data = await docter.findOneAndDelete({ _id: req.params.id });
        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getInfomationByCustomerForDoctorByDoctorEmail = async (req, res) => {
    try {
        const doctor = await docter.findOne({ email: req.params.email });
        const currentDate = new Date();
        const currentDay = currentDate.getDate().toString().padStart(2, '0');
        const currentMonth = (currentDate.getMonth() + 1).toString().padStart(2, '0');
        const currentYear = currentDate.getFullYear().toString();
        // Kiểm tra nếu không tìm thấy bác sĩ
        if (!doctor) {
            throw new Error("Không tìm thấy bác sĩ với email này");
        }

        // Lấy tất cả các `appointment` của bác sĩ
        const appointments = await Appointment.find({ doctorId: doctor._id });

        // Sử dụng `Promise.all` để tìm `record` của từng `appointment`
        const data = await Promise.all(
            appointments.map(async (appointment) => {
                // Tìm `record` tương ứng với `recordId` của `appointment`
                const record = await Record.findOne({ _id: appointment.recordId });

                // Kiểm tra nếu không tìm thấy `record`
                if (!record) {
                    throw new Error(`Không tìm thấy record với id ${appointment.recordId}`);
                }
                // console.log(appointment.day.padStart(2, '0') + "/" + appointment.month + "/" + appointment.year)
                // console.log(currentDay + "/" + currentMonth + "/" + currentYear)
                if (appointment.day.padStart(2, '0') === currentDay && appointment.month.padStart(2, '0')
                    === currentMonth && appointment.year === currentYear) {
                    return {
                        day: appointment.day,
                        month: appointment.month,
                        year: appointment.year,
                        name: record.fullName,
                        timeStart: appointment.timeStart,
                        timeEnd: appointment.timeEnd,
                    };
                }
                return null;
            })
        );
        // console.log(data)
        const filteredData = data.filter((item) => item !== null);
        if (filteredData.length < 0) {
            return res.status(404).json({ message: "No appointment found " });
        }
        res.status(201).json(filteredData);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
const getDocterByEmail = async (req, res) => {
    try {
        // console.log(req.params.email)
        const data = await docter.findOne({ email: req.params.email });
        // console.log(data)
        if (!data) {
            throw new Error("Không tìm thấy bác sĩ với email này");
        }

        if (data.length < 0) {
            return res.status(404).json({ message: "No docter found " });
        }
        res.status(201).json(data);
    } catch (error) {
        return res.status(404).json({
            message: error.message,
        });
    }
};
module.exports = {
    getAllDocter,
    getDocterById,
    createDocter,
    updateDocter,
    deleteDocter,
    getInfomationByCustomerForDoctorByDoctorEmail,
    getDocterByEmail,
    getDocterBySpecId,
};
