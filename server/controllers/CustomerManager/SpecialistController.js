const { json } = require("body-parser");
const specialists = require("../../models/specialists");

const getAllSpecialists = async (req, res) => {
  try {
    const data = await specialists.find();
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const getSpecialistsById = async (req, res) => {
  try {
    const data = await specialists.findOne({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const createSpecialists = async (req, res) => {
  try {
    const data = await specialists(req.body).save();
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const updateSpecialists = async (req, res) => {
  try {
    const data = await specialists.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};
const deleteSpecialists = async (req, res) => {
  try {
    const data = await specialists.findOneAndDelete({ _id: req.params.id });
    if (data.length < 0) {
      return res.status(404).json({ message: "No news found " });
    }
    res.status(201).json(data);
  } catch (error) {
    return res.status(404).json({
      message: error.message,
    });
  }
};

module.exports = {
  getAllSpecialists,
  getSpecialistsById,
  createSpecialists,
  updateSpecialists,
  deleteSpecialists,
};
