const multer = require('multer');
const path = require('path');

// Đường dẫn mặc định lưu ảnh
const IMGPATH = '../../client/public/images';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, IMGPATH));
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const upload = multer({ storage });

module.exports = upload;
