const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const multer = require("./middleware/upload"); // Đường dẫn tới file cấu hình multer
require("dotenv").config();

const authRoute = require("./routes/Account/auth");
const userRoute = require("./routes/Account/user");
const router = require("./routes/CustomerManager/router");
const appointmentRoute = require("./routes/Appointment/PaymentAppointment");
const chatRoute = require('./routes/Chatbot/chatbotRoutes');
const app = express();
const port = process.env.PORT || 5000;

// Cấu hình middlewares
app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);
app.use(bodyParser.json({ limit: "50mb" })); // Tăng giới hạn lên 50MB
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(express.json());
app.use(cookieParser());

// Định nghĩa các tuyến đường
app.use("/api/auth", authRoute);
app.use("/api/user", userRoute);
app.use("/api/appointment", appointmentRoute);
app.use('/api/chatbot', chatRoute);
// Tải lên file hình ảnh
app.post("/upload", multer.single("image"), (req, res) => {
  // Tên trường là 'image'
  if (!req.file) {
    return res.status(400).send("No file uploaded.");
  }
  res.json({ filePath: `images/${req.file.filename}` });
});

router(app);

mongoose
  .connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Successfully connected to the database"))
  .catch((err) =>
    console.error("Could not connect to the database. Exiting now...", err)
  );

app.get("/", (req, res) => {
  res.send(`
    <html>
      <body>
        <h1>Hello SinhToDau!</h1>
        <button onclick="window.location.href = 'exp://192.168.1.61:8081/';">
          Quay về ứng dụng
        </button>
      </body>
    </html>
  `);
});

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
