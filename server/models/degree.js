const mongoose = require('mongoose');

const degreeSchema = new mongoose.Schema({
  degreeContent: { type: String, required: true },
  degreeImage: { type: String }
});

const Degree = mongoose.model('Degree', degreeSchema);
module.exports = Degree;