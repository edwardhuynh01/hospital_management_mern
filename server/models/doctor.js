const { timeEnd } = require("console");
const mongoose = require("mongoose");
const timeSchema = new mongoose.Schema({
  timeStart: {
    type: String,
    required: true,
  },
  timeEnd: {
    type: String,
    required: true,
  },
  isChoose: {
    type: Boolean,
    default: false,
  },
});
const daySchema = new mongoose.Schema({
  day: {
    type: Number,
    required: true,
  },
  times: {
    type: [timeSchema],
    required: true,
  },
});
const doctorSchema = new mongoose.Schema({
  name: { type: String, required: true },
  phoneNumber: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  avatarImage: { type: String },
  experience: { type: String, required: true },
  dateOfWeek: { type: Number, required: true },
  sex: { type: String, required: true },
  days: { type: [daySchema], required: true },
  specialist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Specialist",
    required: true,
  },
  degrees: [{ type: mongoose.Schema.Types.ObjectId, ref: "Degree" }],
});

const Doctor = mongoose.model("Doctor", doctorSchema);
module.exports = Doctor;
