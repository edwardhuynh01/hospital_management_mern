const mongoose = require("mongoose");
const appointmentSchema = new mongoose.Schema({
  id: { type: mongoose.Schema.Types.ObjectId, ref: "Account", required: true },
  recordId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Record",
    required: true,
  },
  dayOfWeek: { type: String, required: true },
  day: { type: String, required: true },
  month: { type: String, required: true },
  year: { type: String, required: true },
  timeStart: { type: String, required: true },
  timeEnd: { type: String, required: true },
  specialistId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Specialists",
    required: true,
  },
  services: { type: String, required: true },
  doctorId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Doctor",
    required: true,
  },
});

const Appointment = mongoose.model("Appointment", appointmentSchema);
module.exports = Appointment;
