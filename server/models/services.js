const mongoose = require("mongoose");

const serviceSchema = new mongoose.Schema({
  name: { type: String, required: true },
  teaser: { type: String },
  description: { type: String },
  mainImage: { type: String },
});

const Service = mongoose.model("Service", serviceSchema);
module.exports = Service;
