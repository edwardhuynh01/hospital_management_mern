const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  id: { type: String, unique: true },
  fullName: { type: String, required: true },
  email: { type: String, unique: true },
  date: { type: Date, required: true },
  sex: { type: String, enum: ["Nam", "Nữ", "Khác"], required: true },
  career: { type: String, required: true },
  address: { type: String, required: true },
  phoneNumber: { type: String, required: true, unique: true },
  Peoples: { type: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

const User = mongoose.model("User", userSchema);
module.exports = User;
