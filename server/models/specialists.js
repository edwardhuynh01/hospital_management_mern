const mongoose = require("mongoose");

const specialistSchema = new mongoose.Schema({
  specialistName: { type: String, required: true },
  specialistDescription: { type: String, required: true },
  specialistImg: { type: String },
  price: { type: Number, require: true },
});

const Specialists = mongoose.model("Specialists", specialistSchema, "specialists");
module.exports = Specialists;
