const mongoose = require("mongoose");

const newsSchema = new mongoose.Schema({
  title: { type: String, required: true },
  teaser: { type: String },
  mainImage: { type: String },
  content: { type: String, required: true },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
});

const News = mongoose.model("News", newsSchema, "news");
module.exports = News;
