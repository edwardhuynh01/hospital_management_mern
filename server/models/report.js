const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema({
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  record: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Record",
    required: true,
  },
  appointment: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Appointment",
      required: true,
    },
  ],
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  amount: { type: Number, required: true },
  isPayment: { type: Boolean, required: true },
  PaymentId: { type: String },
});

const Report = mongoose.model("Report", reportSchema);
module.exports = Report;