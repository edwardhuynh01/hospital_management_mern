const mongoose = require("mongoose");

const logSchema = new mongoose.Schema({
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  operation: {
    type: String,
    enum: ["create", "read", "update", "delete"],
    required: true,
  },
  collection: { type: String, required: true },
  documentId: { type: mongoose.Schema.Types.ObjectId },
  data: { type: mongoose.Schema.Types.Mixed },
  timestamp: { type: Date, default: Date.now },
});

const Log = mongoose.model("Log", logSchema);
module.exports = Log;
