const mongoose = require("mongoose");
const accountSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  phoneNumber: { type: String, required: true, unique: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
  emailComfirmed: { type: Boolean, default: false },
  phoneNumberComfirmed: { type: Boolean, default: false },
  role: {
    type: String,
    enum: ["admin", "customer", "doctor"],
    required: true,
    default: "customer",
  },
});

const Account = mongoose.model("Account", accountSchema);
module.exports = Account;
