const mongoose = require("mongoose");

const recordSchema = new mongoose.Schema({
  fullName: { type: String, required: true },
  email: { type: String },
  country: { type: String },
  date: { type: Date, required: true },
  sex: { type: String, enum: ["Nam", "Nữ", "Khác"], required: true },
  career: { type: String, required: true },
  IdentityCardNumber: { type: String },
  address: { type: String, required: true },
  province: { type: String, required: true },
  district: { type: String, required: true },
  ward: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  Peoples: { type: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  relativesFullName: { type: String },
  relationship: { type: String },
  relativesPhoneNumber: { type: String },
  relativesEmail: { type: String },
});

const Record = mongoose.model("Record", recordSchema);
module.exports = Record;
