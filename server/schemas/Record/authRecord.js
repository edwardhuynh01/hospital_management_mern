const joi = require("joi");
const registerRecordSchema = joi.object({
    fullName: joi.string()
        .required()
        .trim()
        .message({
            'string.base': 'Tên phải là một chuỗi ký tự',
            'any.required': 'Tên là bắt buộc',
            'string.empty': 'Tên không được để trống',
        }),
    email: joi.string()
        .email()
        .required()
        .messages({
            'string.base': 'Email phải là một chuỗi ký tự',
            'any.required': 'Email là bắt buộc',
            'string.email': 'Email không hợp lệ',
            'string.empty': 'Email không được để trống',
        }),
    date: joi.date().required().messages({
        'any.required': 'Ngày sinh là bắt buộc',
    }),
    sex: joi.string().valid(...['Nam', 'Nữ', 'Khác']).required().messages({
        'any.required': 'Giới tính là bắt buộc',
        'any.only': 'Giới tính phải là "Nam", "Nữ", hoặc "Khác"',
    }),
    career: joi.string().required().messages({
        'any.required': 'Nghề nghiệp là bắt buộc',
        'string.empty': 'Nghề nghiệp không được để trống',
    }),
    address: joi.string().required().messages({
        'any.required': 'Địa chỉ là bắt buộc',
        'string.empty': 'Địa chỉ không được để trống',
    }),
    phoneNumber: joi.string()
        .required()
        .messages({
            'any.required': 'Số điện thoại là bắt buộc',
            'string.empty': 'Số điện thoại không được để trống',
        }),
    Peoples: oi.string()
        .required()
        .trim()
        .message({
            'string.base': 'Tên phải là một chuỗi ký tự',
        })
});
module.exports = registerRecordSchema;